<?php

use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\HumanResources;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\UpdateLogic;
use Illuminate\Support\Facades\Route;

Route::get('PromotingPartners', [FrontEndController::class, 'PromotingPartners'])->name('PromotingPartners');

Route::get('ResearchDetails', [FrontEndController::class, 'ResearchDetails'])->name('ResearchDetails');

Route::get('Reports', [FrontEndController::class, 'Reports'])->name('Reports');

Route::get('PolicyDetails', [FrontEndController::class, 'PolicyDetails'])->name('PolicyDetails');

Route::middleware(['auth'])->group(function () {

    Route::get('Partners', [FrontEndController::class, 'Partners'])->name('Partners');

    Route::get('affiliate', [FrontEndController::class, 'affiliate'])->name('affiliate')
    ;
    Route::get('Assoc', [FrontEndController::class, 'Assoc'])->name('Assoc');

    Route::get('Founders', [FrontEndController::class, 'Founders'])->name('Founders');

    Route::get('Blog', [FrontEndController::class, 'Blog'])->name('Blog');

    Route::get('News', [FrontEndController::class, 'News'])->name('News');

    Route::get('Download', [FrontEndController::class, 'Download'])->name('Download');

    Route::get('jobs', [FrontEndController::class, 'jobs'])->name('jobs');

    Route::get('Gallery', [FrontEndController::class, 'Gallery'])->name('Gallery');

    Route::get('Directors', [FrontEndController::class, 'Directors'])->name('Directors');

    Route::get('PolicyBriefs', [FrontEndController::class, 'PolicyBriefs'])->name('PolicyBriefs');

    Route::get('MgtReports', [FrontEndController::class, 'MgtReports'])->name('MgtReports');

    Route::get('MgtResearch', [FrontEndController::class, 'MgtResearch'])->name('MgtResearch');

    Route::get('SecTeam', [FrontEndController::class, 'SecTeam'])->name('SecTeam');

    Route::get('OurWorks', [FrontEndController::class, 'OurWorks'])->name('OurWorks');

    Route::post('NewRecord', [FrontEndController::class, 'NewRecord'])->name('NewRecord');

    Route::get('ProgramAreas', [FrontEndController::class, 'ProgramAreas'])->name('ProgramAreas');

    Route::post('NewSlider', [FrontEndController::class, 'NewSlider'])->name('NewSlider');

    Route::get('MgtSlider', [FrontEndController::class, 'MgtSlider'])->name('MgtSlider');

    Route::get('DeleteDoc/{id}', [InventoryController::class, 'DeleteDoc'])->name('DeleteDoc');

    Route::post('UpdateDocs', [InventoryController::class, 'UpdateDocs'])->name('UpdateDocs');

    Route::post('AddFile', [InventoryController::class, 'AddFile'])->name('AddFile');

    Route::get('MgtInvFiles', [InventoryController::class, 'MgtInvFiles'])->name('MgtInvFiles');

    Route::post('DocSelected', [InventoryController::class, 'DocSelected'])->name('DocSelected');

    Route::get('SelectDocCat', [InventoryController::class, 'SelectDocCat'])->name('SelectDocCat');

    Route::get('CheckoutReport/{Month}/{Year}', [InventoryController::class, 'CheckoutReport'])->name('CheckoutReport');

    Route::post('NewInvDocCat', [InventoryController::class, 'NewInvDocCat'])->name('NewInvDocCat');

    Route::get('InvDocCats', [InventoryController::class, 'InvDocCats'])->name('InvDocCats');

    Route::get('Depleted', [InventoryController::class, 'Depleted'])->name('Depleted');

    Route::post('ConvertToGet', [InventoryController::class, 'ConvertToGet'])->name('ConvertToGet');

    Route::get('ReportQuery', [InventoryController::class, 'ReportQuery'])->name('ReportQuery');

    Route::get('/Approve/{id}', [InventoryController::class, 'Approve'])->name('Approve');

    Route::get('/AppReq', [InventoryController::class, 'AppReq'])->name('AppReq');

    Route::post('/NewReq', [InventoryController::class, 'NewReq'])->name('NewReq');

    Route::get('/MyReq', [InventoryController::class, 'MyReq'])->name('MyReq');

    Route::post('/NewItem', [InventoryController::class, 'NewItem'])->name('NewItem');

    Route::get('/MgtItems', [InventoryController::class, 'MgtItems'])->name('MgtItems');

    Route::post('/NewInvCat', [InventoryController::class, 'NewInvCat'])->name('NewInvCat');

    Route::get('/MgtInvCat', [InventoryController::class, 'MgtInvCat'])->name('MgtInvCat');

    Route::post('/NewStaff', [HumanResources::class, 'NewStaff'])->name('NewStaff');

    Route::get('/MgtStaff', [HumanResources::class, 'MgtStaff'])->name('MgtStaff');

    Route::post('/NewRole', [HumanResources::class, 'NewRole'])->name('NewRole');

    Route::get('/MgtRoles', [HumanResources::class, 'MgtRoles'])->name('MgtRoles');

    Route::post('/UpdateLogic', [UpdateLogic::class, 'UpdateLogic'])->name('UpdateLogic');

    Route::post('/CMSUpdate', [UpdateLogic::class, 'CMSUpdate'])->name('CMSUpdate');

    Route::get('/MassDelete/{id}/{TableName}', [UpdateLogic::class, 'MassDelete'])->name('MassDelete');

    Route::post('/UpdateDept', [HumanResources::class, 'UpdateDept'])->name('UpdateDept');

    Route::get('/DelDept/{id}', [HumanResources::class, 'DelDept'])->name('DelDept');

    Route::post('/NewDept', [HumanResources::class, 'NewDept'])->name('NewDept');

    Route::get('/MgtDepts', [HumanResources::class, 'MgtDepts'])->name('MgtDepts');

    Route::get('/VirtualOffice', [InventoryController::class, 'VirtualOffice'])->name('home');

    Route::get('/dashboard', [InventoryController::class, 'VirtualOffice'])->name('dashboard');

    Route::post('/UpdatePassword', [MainController::class, 'UpdatePassword'])->name('UpdatePassword');
});
Route::get('OurWork', [FrontEndController::class, 'OurWork'])->name('OurWork');

Route::get('TheAfriChildCenterBlogPost/{AfriChildBlogPost}', [FrontEndController::class, 'TheAfriChildCenterBlogPost'])->name('TheAfriChildCenterBlogPost');

Route::get('BlogDetails', [FrontEndController::class, 'BlogDetails'])->name('BlogDetails');

Route::get('/', function () {

    return redirect()->route('Home');

});

//Route::get('/', [FrontEndController::class, 'Home']);
Route::get('/offline', [FrontEndController::class, 'Home']);
Route::get('Home', [FrontEndController::class, 'Home'])->name('Home');

require __DIR__ . '/auth.php';
