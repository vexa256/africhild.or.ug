<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" role="button"
        aria-expanded="false">
        <span class="nav-link-icon d-md-none d-lg-inline-block">
            <i class="fas fa-users" aria-hidden="true"></i>
        </span>
        <span class="nav-link-title">
            Staff Details
        </span>
    </a>
    <div class="dropdown-menu">
        <div class="dropdown-menu-columns">
            <div class="dropdown-menu-column">
                <a class="dropdown-item" href="{{ route('MgtDepts') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Departments
                </a>

                <a class="dropdown-item" href="{{ route('MgtRoles') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Staff Roles
                </a>

                <a class="dropdown-item" href="{{ route('MgtStaff') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Staff Members
                </a>



            </div>
        </div>
    </div>
</li>
