<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" role="button"
        aria-expanded="false">
        <span class="nav-link-icon d-md-none d-lg-inline-block">
            <i class="fas fa-store-alt" aria-hidden="true"></i>
        </span>
        <span class="nav-link-title">
            Inventory
        </span>
    </a>
    <div class="dropdown-menu">
        <div class="dropdown-menu-columns">
            <div class="dropdown-menu-column">
                <a class="dropdown-item" href="{{ route('MgtInvCat') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Item Categories
                </a>

                <a class="dropdown-item" href="{{ route('MgtItems') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Inventory Database
                </a>


                <a class="dropdown-item" href="{{ route('AppReq') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Approve Requests
                </a>

                <a class="dropdown-item" href="{{ route('InvDocCats') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> File Categories
                </a>
                <a class="dropdown-item" href="{{ route('SelectDocCat') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Inventory Files
                </a>




            </div>
        </div>
    </div>
</li>

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" role="button"
        aria-expanded="false">
        <span class="nav-link-icon d-md-none d-lg-inline-block">
            <i class="fas fa-chart-area" aria-hidden="true"></i>
        </span>
        <span class="nav-link-title">
            Inventory Reports
        </span>
    </a>
    <div class="dropdown-menu">
        <div class="dropdown-menu-columns">
            <div class="dropdown-menu-column">
                <a class="dropdown-item" href="{{ route('Depleted') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Depleted Items
                </a>

                <a class="dropdown-item" href="{{ route('ReportQuery') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Checkout Report
                </a>


            </div>
        </div>
    </div>
</li>
