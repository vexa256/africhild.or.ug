<li class="nav-item dropdown Cms_Admin">
    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" role="button"
        aria-expanded="false">
        <span class="nav-link-icon d-md-none d-lg-inline-block">
            <i class="fas fa-wrench" aria-hidden="true"></i>
        </span>
        <span class="nav-link-title">
            CMS Panel
        </span>
    </a>
    <div class="dropdown-menu">
        <div class="dropdown-menu-columns">
            <div class="dropdown-menu-column">
                <a class="dropdown-item" href="{{ route('MgtSlider') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Slider Settings
                </a>

                <a class="dropdown-item" href="{{ route('ProgramAreas') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Program Areas
                </a>

                <a class="dropdown-item" href="{{ route('OurWorks') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Our Work
                </a>

                <a class="dropdown-item" href="{{ route('MgtResearch') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Research
                </a>

                <a class="dropdown-item" href="{{ route('MgtReports') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Reports
                </a>

                <a class="dropdown-item" href="{{ route('Partners') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Promiting Partners
                </a>

                <a class="dropdown-item" href="{{ route('SecTeam') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Secretariat Team
                </a>

                <a class="dropdown-item" href="{{ route('PolicyBriefs') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Policy Briefings
                </a>

                <a class="dropdown-item" href="{{ route('Directors') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Board Of Directors
                </a>

                <a class="dropdown-item" href="{{ route('Gallery') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Gallery
                </a>

                <a class="dropdown-item" href="{{ route('jobs') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Post a Job
                </a>

                <a class="dropdown-item" href="{{ route('Download') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Download Resources
                </a>

                <a class="dropdown-item" href="{{ route('News') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> News
                </a>

                <a class="dropdown-item" href="{{ route('Blog') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Blog
                </a>

                <a class="dropdown-item" href="{{ route('Founders') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Founders
                </a>


                <a class="dropdown-item" href="{{ route('Assoc') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i> Research Associates
                </a>

                <a class="dropdown-item" href="{{ route('affiliate') }}">
                    <i class="fas me-2 fa-circle" aria-hidden="true"></i>Affiliates
                </a>









            </div>
        </div>
    </div>
</li>
