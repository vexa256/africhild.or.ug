<div class="modal modal-blur fade" id="UpdateAccount" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hello {{ Auth::user()->name }}, Use this form to update your account
                    password</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="{{ route('UpdatePassword') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label">New Password</label>
                        <input required type="password" class="form-control" name="password">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Confirm Password</label>
                        <input required type="padssword" class="form-control" name="password_confirmation">
                    </div>

                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">








                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
