<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>
        @isset($Title)
            {{ $Title }}
        @endisset
    </title>
    <!-- CSS files -->
    <link href="{{ asset('dist/css/tabler.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('js/photoviewer.min.css') }}" rel="stylesheet" />


    <link href="{{ asset('dist/css/demo.min.css') }}" rel="stylesheet" />

    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
        rel="stylesheet" />

    <link rel="stylesheet" type="text/css"
        href="{{ asset('dt/datatables.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/flexselect.css') }}">

    <link href="{{ asset('assets/sumernote/summernote-lite.min.css') }}"
        rel="stylesheet" />

    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet" />

    <style>
        .note-editable {

            background-color: white !important;

        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #df0a81;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #df0a81;
        }

        textarea {
            height: 200px !important;
        }
    </style>

    @if (Auth::user()->Privileges != 'Super_Admin')
        <style>
            .deleteConfirm {

                display: none !important;
            }

            .Cms_Admin {

                display: none !important;
            }
        </style>
    @endif

    <style>
        .select2-selection {

            display: none !important;
        }


        .sec-padding {

            padding-top: 60px !important;
        }
    </style>
</head>

<body class="antialiased">
    <div class="wrapper">
