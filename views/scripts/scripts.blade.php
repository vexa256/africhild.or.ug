<!-- Tabler Core -->
<script src="{{ asset('dist/js/tabler.min.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/flexselect.js') }}"></script>

<script type="text/javascript" src="{{ asset('dt/datatables.min.js') }}"></script>

<script src="{{ asset('js/lightbox.js') }}"></script>

<script src="{{ asset('js/swal2.js') }}"></script>

@include('nots.notifications')

@isset($Edit)
    <script src="{{ asset('assets/sumernote/summernote-lite.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('textarea').summernote({

                height: 200
            });
        });
    </script>
@endisset


@isset($PDFJS)
    <script defer src="https://documentcloud.adobe.com/view-sdk/main.js"></script>

    <script type="text/javascript" src="{{ asset('js/pdf.js') }}"></script>
@endisset
<script src="https://cdn.jsdelivr.net/npm/litepicker/dist/bundle.js"></script>
<script src="{{ asset('js/litepickerinit.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
</body>

</html>
