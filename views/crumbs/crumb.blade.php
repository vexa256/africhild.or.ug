<div class="container-xl">
    <!-- Page title -->
    <div class="page-header d-print-none">
        <div class="row align-items-center">
            <div class="col">

                <h2 class="page-title">
                    @isset($Desc)
                        {{ $Title }}
                    @endisset
                </h2>
            </div>
            <!-- Page title actions -->

        </div>
    </div>
</div>
