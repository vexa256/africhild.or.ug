<?php

/**
 * @see https://github.com/artesaos/seotools
 */

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'              => [
            'title'     => "AfriChild | The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children.", // set false to total remove
            'titleBefore' => false, // Put defaults.title before page title, like 'It's Over 9000! - Dashboard'
            'description' => 'The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children\'s wellbeing is realized for sustainable development. | The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children\'s wellbeing is realized for sustainable development. ', // set false to total remove
            'separator' => ' - ',
            'keywords'  => ['africhild , The AfriChild Centre exists,  African Children, The AfriChild Centre, Kampala, Uganda, Makerere University , wellbeing is realized for sustainable development, Makerere University, Mary Stuart Rd, Kampala , We strive for an Africa where children\'s wellbeing'],
            'canonical' => 'https://africhild.or.ug/', // Set to null or 'full' to use 'https://africhild.or.ug/', set to 'current' to use 'https://africhild.or.ug/', set false to total remove
            'robots'      => 'all', // Set to 'all', 'none' or any combination of index/noindex and follow/nofollow
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags'        => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
            'norton'    => null,
        ],

        'add_notranslate_class' => false,
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'     => 'AfriChild | The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children.', // set false to total remove
            'description' => 'The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children\'s wellbeing is realized for sustainable development. | The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children\'s wellbeing is realized for sustainable development. ', // set false to total remove
            'url' => 'https://africhild.or.ug/', // Set null for using 'https://africhild.or.ug/', set false to total remove
            'type' => false,
            'site_name' => 'The AfriChild Centre',
            'images'    => ['https://africhild.or.ug/logo.png'],
        ],
    ],
    'twitter'   => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            'card' => 'summary',
            'site' => '@AfriChildCenter',
        ],
    ],
    'json-ld'   => [
        /*
         * The default configurations to be used by the json-ld generator.
         */
        'defaults' => [
            'title'  => 'AfriChild | The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children.', // set false to total remove
            'description' => 'The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children\'s wellbeing is realized for sustainable development. | The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children\'s wellbeing is realized for sustainable development.', // set false to total remove
            'url' => 'https://africhild.or.ug/', // Set to null or 'full' to use 'https://africhild.or.ug/', set to 'current' to use 'https://africhild.or.ug/', set false to total remove
            'type' => 'WebPage',
            'images' => ['https://africhild.or.ug/logo.png'],
        ],
    ],
];
