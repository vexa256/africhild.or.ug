var staticCacheName = "pwa-v" + new Date().getTime();
var MyCache = 'now';
var filesToCache = [
    '/offline',
    '/css/style.css',
    '/css/responsive.css',
    '/https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css',
    '/css/app.css',
    '/js/app.js',
    '/js/jquery-1.11.1.min.js',
    '/js/bootstrap.min.js',
    '/js/jquery.bxslider.min.js',
    '/js/owl.carousel.min.js',
    '/js/jquery-parallax.js',
    '/js/validate.js',
    '/js/jquery.mixitup.min.js',
    '/js/jquery.fancybox.pack.js',
    '/js/jquery.easing.min.js',
    '/js/circle-progress.js',
    '/js/jquery.appear.js',
    '/js/jquery.countTo.js',
    '/js/isotope.pkgd.min.js',
    '/js/jquery-ui-1.11.4/jquery-ui.js',
    '//cdn.jsdelivr.net/npm/sweetalert2@11',
    '/jrevolution/js/jquery.themepunch.tools.min.js',
    '/revolution/js/jquery.themepunch.revolution.min.js',
    '/revolution/js/extensions/revolution.extension.actions.min.js',
    '/revolution/js/extensions/revolution.extension.carousel.min.js',
    '/revolution/js/extensions/revolution.extension.kenburn.min.js',
    '/revolution/js/extensions/revolution.extension.layeranimation.min.js',
    '/revolution/js/extensions/revolution.extension.migration.min.js',
    '/revolution/js/extensions/revolution.extension.navigation.min.js',
    '/revolution/js/extensions/revolution.extension.parallax.min.js',
    '/revolution/js/extensions/revolution.extension.slideanims.min.js',



    '/revolution/js/extensions/revolution.extension.video.min.js',
    'https://cdn.tiny.cloud/1/1nr3t3t5xeyg86kk7vb6p7u0d9eo1w4zd7dy14p1volsp9ed/tinymce/5/tinymce.min.js',
  
    


    '/logo.png',
    '/images/icons/icon-72x72.png',
    '/images/icons/icon-96x96.png',
    '/images/icons/icon-128x128.png',
    '/images/icons/icon-144x144.png',
    '/images/icons/icon-152x152.png',
    '/images/icons/icon-192x192.png',
    '/images/icons/icon-384x384.png',
    '/images/icons/icon-512x512.png',
];

/**My fUNC */
 const ClearDump = () => {

        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        });
    


 }
/**My fUNC */
 const ClearFunc = () => {


    const cacheName = 'my-app-v1';

    self.addEventListener('activate', async (event) => {

    const existingCaches = await caches.keys();
    const invalidCaches = existingCaches.filter(c => c !== cacheName);
    await Promise.all(invalidCaches.map(ic => caches.delete(ic)));

    // do whatever else you need to...

});
 }

// Cache on install
self.addEventListener("install", event => {

    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match('offline');
            })
    )
});
