window.addEventListener("DOMContentLoaded", (event) => {

    $("[data-gallery=example]").click(function (e) {
        e.preventDefault();

        var items = [],
            options = {
                index: $(this).index(),
            };

        $("[data-gallery=manual]").each(function () {
            let src = $(this).attr("href");
            items.push({
                src: src,
            });
        });
        new PhotoViewer(items, options);
    });

    jQuery("select.flexselect").flexselect();

    $(document).on("click", ".deleteConfirm", function () {
        var route = $(this).data("route");
        var msg = $(this).data("msg");

        Swal.fire({
            title: "Are you sure?",
            text: msg,
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes",
        }).then((result) => {
            if (result.isConfirmed) {
                window.location = route;
            }
        });
    });

    $(document).on("click", ".RestockConfirm", function () {
        Swal.fire({
            title: "Are you sure?",
            text: "You want to re-stock this item, This action is not reversible!!",
            icon: "warning",
            showCancelButton: true,
            showConfirmButton: false,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            cancelButtonText: "I Understand ",
        }).then((result) => {
            if (result.isConfirmed) {
            }
        });
    });

    $(document).on("click", ".deleteConfirm2", function () {
        var route = $(this).data("route");
        var msg = $(this).data("msg");

        Swal.fire({
            title: "Are you sure?",
            text: msg,
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes",
        }).then((result) => {
            if (result.isConfirmed) {
                window.location = route;
            }
        });
    });

    $(".mytable").DataTable({
        paging: true,
        lengthChange: true,
        searching: true,
        pageLength: 5,
        ordering: true,
        info: true,
        autoWidth: true,
        responsive: true,
        dom: "Bfrtip",

        buttons: ["excel", "pdf", "print"],
    });

    const UpdateUI = () => {
        $(".paginate_button").addClass(
            "bg-dark text-light shadow-lg btn btn-sm"
        );
        $(".dt-button").addClass("btn btn-sm text-light bg-primary mb-3");
        $(".dt-buttons").addClass("px-1 py-1 text-light");
        $(".DataTables_Table_0_filter").addClass("px-1 py-1 text-light");
    };

    /*  UpdateUI();

   /* setInterval(function () {
        UpdateUI();
    }, 1000);*/
});

/****JQuery plugin to block input of text in number fields with a class . IntOnlyNow */
(function (a) {
    a.fn.extend({
        inputNumberFormat: function (c) {
            this.defaultOptions = {
                decimal: 2,
                decimalAuto: 2,
                separator: ".",
                separatorAuthorized: [".", ","],
                allowNegative: false,
            };
            var e = a.extend({}, this.defaultOptions, c);
            var d = function (i, f) {
                var h = [];
                var g = "^[0-9]+";
                if (f.allowNegative) {
                    g = "^-{0,1}[0-9]*";
                }
                if (f.decimal) {
                    g +=
                        "[" +
                        f.separatorAuthorized.join("") +
                        "]?[0-9]{0," +
                        f.decimal +
                        "}";
                    g = new RegExp(g + "$");
                    h = i.match(g);
                    if (!h) {
                        g =
                            "^[" +
                            f.separatorAuthorized.join("") +
                            "][0-9]{0," +
                            f.decimal +
                            "}";
                        g = new RegExp(g + "$");
                        h = i.match(g);
                    }
                } else {
                    g = new RegExp(g + "$");
                    h = i.match(g);
                }
                return h;
            };
            var b = function (k, f) {
                var j = k;
                if (!j) {
                    return j;
                }
                if (j == "-") {
                    return "";
                }
                j = j.replace(",", f.separator);
                if (f.decimal && f.decimalAuto) {
                    j =
                        Math.round(j * Math.pow(10, f.decimal)) /
                            Math.pow(10, f.decimal) +
                        "";
                    if (j.indexOf(f.separator) === -1) {
                        j += f.separator;
                    }
                    var h = f.decimalAuto - j.split(f.separator)[1].length;
                    for (var g = 1; g <= h; g++) {
                        j += "0";
                    }
                }
                return j;
            };
            return this.each(function () {
                var f = a(this);
                f.on("keypress", function (j) {
                    if (j.ctrlKey) {
                        return;
                    }
                    if (j.key.length > 1) {
                        return;
                    }
                    var i = a.extend({}, e, a(this).data());
                    var g = a(this).val().substr(0, j.target.selectionStart);
                    var h = a(this)
                        .val()
                        .substr(
                            j.target.selectionEnd,
                            a(this).val().length - 1
                        );
                    var k = g + j.key + h;
                    if (!d(k, i)) {
                        j.preventDefault();
                        return;
                    }
                });
                f.on("blur", function (h) {
                    var g = a.extend({}, e, a(this).data());
                    a(this).val(b(a(this).val(), g));
                });
                f.on("change", function (h) {
                    var g = a.extend({}, e, a(this).data());
                    a(this).val(b(a(this).val(), g));
                });
            });
        },
    });
})(jQuery);
/***Plugins */
if ($(".IntOnlyNow").length > 0) {
    $(".IntOnlyNow").inputNumberFormat();
}





