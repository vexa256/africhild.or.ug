document.addEventListener("adobe_dc_view_sdk.ready", function () {
    $(document).on("click", ".PdfViewer", function () {
        var path = $(this).data("source");
        var doc = $(this).data("doc");

        var adobeDCView = new AdobeDC.View({
            clientId: "ca9c26ff39344f42b0727de301b10ac3",
            divId: "adobe-dc-view",
        });
        adobeDCView.previewFile(
            {
                content: { location: { url: path } },
                metaData: { fileName: doc },
            },
            {}
        );
    });
});
