<?php

namespace App\Http\Controllers;

use DB;

class InsertData extends Controller
{
    public function InsertData($request)
    {

        $validated = $this->validate($request, [
            '*' => 'nullable',
        ]);

        if ($request->hasFile('URL') && !$request->hasFile('Thumbnail')) {

            // $up = DB::table($request->TableName)->where('UID',  $request->UID)->first();

            //unlink(public_path($up->URL));

            $FileName = time() . '.' . $request->URL->extension();

            $request->URL->move(public_path('assets/docs'), $FileName);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            DB::table($request->TableName)->where('UID', $request->UID)->update([

                'URL' => 'assets/docs/' . $FileName,
            ]);

            return redirect()->back()->with('status', 'The selected record was updated successfully');

        } elseif ($request->hasFile('Thumbnail') && !$request->hasFile('URL')) {

            //  $up = DB::table($request->TableName)->where('UID',  $request->UID)->first();

            //  unlink(public_path($up->Thumbnail));

            $FileName = time() . '.' . $request->Thumbnail->extension();

            $request->Thumbnail->move(public_path('assets/docs'), $FileName);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );
            DB::table($request->TableName)->where('UID', $request->UID)->update([

                'Thumbnail' => 'assets/docs/' . $FileName,
            ]);

            return redirect()->back()->with('status', 'The selected record was updated successfully');

        } elseif ($request->hasFile('Thumbnail') && $request->hasFile('URL')) {

            // $up = DB::table($request->TableName)->where('UID',  $request->UID)->first();

            // unlink(public_path($up->Thumbnail));

            $FileName1 = time() . '.' . $request->Thumbnail->extension();

            $request->Thumbnail->move(public_path('assets/docs'), $FileName1);

            ///  unlink(public_path($up->URL));

            $FileName = time() . '.' . $request->URL->extension();

            $request->URL->move(public_path('assets/docs'), $FileName);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            DB::table($request->TableName)->where('UID', $request->UID)->update([

                'Thumbnail' => 'assets/docs/' . $FileName1,
                'URL' => 'assets/docs/' . $FileName,
            ]);

            return redirect()->back()->with('status', 'The selected record was updated successfully');

        } else {

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            return redirect()->back()->with('status', 'The selected record was updated successfully');
        }

    }

}
