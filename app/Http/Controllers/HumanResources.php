<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Departments;
use App\Models\Roles;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class HumanResources extends Controller
{

    public function MgtDepts()
    {

        $Depts = Departments::all();

        $data = [
            'Page'  => 'departments.MgtDepts',
            'Title' => 'Manage all tracked departments ',
            'Desc'  => 'Department Settings',
            'Depts' => $Depts,
        ];

        return view('index', $data);
    }

    public function NewDept(Request $request)
    {
        $validated = $this->validate($request, [
            'Department' => 'required|unique:departments',
            'DID'        => 'required|unique:departments',

        ]);

        Departments::create($validated);

        return redirect()->back()->with('status', 'New Department Created successfully');
    }

    public function DelDept($id)
    {
        Departments::find($id)->delete();

        return redirect()->back()->with('status', 'New Department Deleted successfully');

    }

    public function UpdateDept(Request $request)
    {
        $validated = $this->validate($request, [
            'Department' => 'required',
            'id'         => 'required',

        ]);

        $Departments             = Departments::find($request->id);
        $Departments->Department = $request->Department;
        $Departments->save();

        return redirect()->back()->with('status', 'The selected record was updated successfully');

    }

    public function MgtRoles()
    {
        $Roles = Roles::all();

        $data = [
            'Page'  => 'roles.MgtRoles',
            'Title' => 'Manage all assignable staff roles ',
            'Desc'  => 'Staff role Settings',
            'Roles' => $Roles,
        ];

        return view('index', $data);
    }

    public function NewRole(Request $request)
    {
        $validated = $this->validate($request, [
            'Role' => 'required|unique:roles',
            'RID'  => 'required|unique:roles',

        ]);

        Roles::create($validated);

        return redirect()->back()->with('status', 'New Staff Role Created successfully');
    }

    public function MgtStaff()
    {

        $countRoles       = Roles::count();
        $countDepartments = Departments::count();

        $Depts = Departments::all();
        $Roles = Roles::all();

        if ($countRoles < 1) {

            return redirect()->back()->with('error_a', 'No staff roles detected, Please add staff roles first and try again');

        } elseif ($countDepartments < 1) {

            return redirect()->back()->with('error_a', 'No  departments detected, Please add staff roles first and try again');
        }

        $StaffDetails = DB::table('staff AS S')

            ->join('roles AS R', 'R.RID', '=', 'S.RID')

            ->join('departments AS D', 'D.DID', '=', 'S.DID')

            ->select('D.Department', 'S.*', 'R.Role')

            ->get();

        $data = [
            'Page'         => 'staff.MgtStaff',
            'Title'        => 'Manage all trackable staff members ',
            'Desc'         => 'Staff record Settings',
            'StaffDetails' => $StaffDetails,
            'Roles'        => $Roles,
            'Depts'        => $Depts,
        ];

        return view('index', $data);

    }

    public function NewStaff(Request $request)
    {

        $validated = $this->validate($request, [
            'RID'        => 'required',
            'DID'        => 'required',
            'Name'       => 'required|unique:staff',
            'Privileges' => 'required',
            'EmpID'      => 'required',
            'Email'      => 'required|email',

        ]);

        DB::table('staff')->insert(
            $request->except([
                '_token',

            ])
        );

        $User = new User;

        $User->name       = $request->Name;
        $User->email      = $request->Email;
        $User->password   = \Hash::make($request->Email);
        $User->EmpID      = $request->EmpID;
        $User->Privileges = $request->Privileges;

        $User->save();

        return redirect()->back()->with('status', 'New Staff Member Created successfully');

    }
}
