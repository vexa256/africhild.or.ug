<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TimeFix;
use App\Models\ItemCategories;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class InventoryController extends Controller
{

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $TimeFix = new TimeFix;

        $TimeFix->FixTimestamps();
    }

    public function VirtualOffice(Type $var = null)
    {
        if (\Auth::user()->Privileges == 'Super_Admin' || \Auth::user()->Privileges == 'Admin') {
            return redirect()->route('MgtStaff');

        } else {
            return redirect()->route('MyReq');
        }
    }

    public function MgtInvCat()
    {

        $Cats = ItemCategories::all();

        $data = [
            'Page'  => 'categories.MgtCat',
            'Title' => 'Manage broad categories applied to consumables ',
            'Desc'  => 'Broad Category Settings',
            'Cats'  => $Cats,
        ];

        return view('index', $data);
    }

    public function NewInvCat(Request $request)
    {

        $validated = $this->validate($request, [
            'CID'        => 'required|unique:item_categories',
            'Category'   => 'required|unique:item_categories',
            'Unit'       => 'required',
            'created_at' => 'required',

        ]);

        DB::table('item_categories')->insert(
            $request->except([
                '_token',

            ])
        );

        return redirect()->back()->with('status', 'New Inventory Category  Created successfully');
    }

    public function MgtItems()
    {
        $CategoryCount = ItemCategories::count();

        if ($CategoryCount < 1) {

            return redirect()->back()->with('error_a', 'No item categories detected, Please add them first and try again');

        }

        $Stock = DB::table('items AS I')

            ->join('item_categories AS C', 'C.CID', '=', 'I.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'I.EmpID')

            ->select('I.*', 'C.Category', 'C.Unit', 'S.Name')

            ->get();

        $Cats = ItemCategories::all();

        $data = [
            'Page'  => 'stock.MgtStock',
            'Title' => 'Manage the inventory database',
            'Desc'  => 'Consumable\'s database',
            'Stock' => $Stock,
            'Cats'  => $Cats,
        ];

        return view('index', $data);
    }
    public function NewItem(Request $request)
    {
        $validated = $this->validate($request, [
            'CID'           => 'required|unique:items',
            'Item'          => 'required|unique:items',
            'IID'           => 'required|unique:items',
            'created_at'    => 'required',
            'BriefDesc'     => 'required',
            'Qty'           => 'required',
            'WarningAmount' => 'required',
            'EmpID'         => 'required',

        ]);

        DB::table('items')->insert(
            $request->except([
                '_token',

            ])
        );

        return redirect()->back()->with('status', 'New Inventory Item  Created successfully');
    }

    public function MyReq()
    {

        $Items = DB::table('items')->get();

        $Req = DB::table('requests AS R')

            ->where('R.RequestedBy', '=', \Auth::user()->EmpID)

            ->join('item_categories AS C', 'C.CID', '=', 'R.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'R.RequestedBy')

            ->select('R.*', 'C.Category', 'C.Unit', 'S.Name')

            ->orderBy('R.created_at', 'desc')

            ->get();

        $data = [
            'Page'  => 'request.MyReq',
            'Title' => 'View all inventory request made by ' . \Auth::user()->name,
            'Desc'  => 'Consumable request log',
            'Req'   => $Req,
            'Items' => $Items,

        ];

        return view('index', $data);
    }

    public function NewReq(Request $request)
    {
        $validated = $this->validate($request, [

            'IID'          => 'required',
            'BriefDesc'    => 'required',
            'QtyRequested' => 'required',
            'RequestedBy'  => 'required',

        ]);

        $Item = DB::table('items')->where('IID', $request->IID)->first();

        DB::table('requests')->insert([
            'Item'           => $Item->Item,
            'IID'            => $Item->IID,
            'CID'            => $Item->CID,
            'BriefDesc'      => $request->BriefDesc,
            'QtyRequested'   => $request->QtyRequested,
            'RequestedBy'    => \Auth::user()->EmpID,
            'created_at'     => $request->created_at,
            'ApprovalStatus' => 'pending',

        ]);

        return redirect()->back()->with('status', 'New consumable request  sent to approver  successfully');

    }

    public function AppReq()
    {

        $Items = DB::table('items')->get();

        $Req = DB::table('requests AS R')

            ->where('R.ApprovalStatus', '=', 'pending')

            ->join('item_categories AS C', 'C.CID', '=', 'R.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'R.RequestedBy')

            ->select('R.*', 'C.Category', 'C.Unit', 'S.Name')

            ->orderBy('R.created_at', 'desc')

            ->get();

        $App = DB::table('requests AS R')

            ->where('R.ApprovalStatus', '=', 'approved')

            ->join('item_categories AS C', 'C.CID', '=', 'R.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'R.RequestedBy')

            ->select('R.*', 'C.Category', 'C.Unit', 'S.Name')

            ->orderBy('R.created_at', 'desc')

            ->get();

        $Dec = DB::table('requests AS R')

            ->where('R.ApprovalStatus', '!=', 'approved')

            ->where('R.ApprovalStatus', '!=', 'pending')

            ->join('item_categories AS C', 'C.CID', '=', 'R.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'R.RequestedBy')

            ->select('R.*', 'C.Category', 'C.Unit', 'S.Name')

            ->orderBy('R.created_at', 'desc')

            ->get();

        $data = [
            'Page'  => 'request.Approve',
            'Title' => 'Approve/Decline all pending consumable requests ',
            'Desc'  => 'Consumable request management log',
            'Req'   => $Req,
            'Items' => $Items,
            'App'   => $App,
            'Dec'   => $Dec,

        ];

        return view('index', $data);
    }

    public function Approve($id)
    {
        $Up = DB::table('requests')->where('id', $id)->first();

        $ReqItem = DB::table('Items')->where('IID', $Up->IID)->first();

        if ($ReqItem->Qty >= $Up->QtyRequested) {

            $NewQty = $ReqItem->Qty - $Up->QtyRequested;

            DB::table('Items')->where('IID', $Up->IID)->update([

                "Qty" => $NewQty,
            ]);

            DB::table('requests')->where('id', $id)->update([
                'ApprovalStatus' => 'approved',
            ]);

            return redirect()->back()->with('status', ' Consumable request approved   successfully');
        } else {

            return redirect()->back()->with('error_a', ' Requested consumable quantity is greater than whats available in stock, Re-stock and try again');
        }
    }

    public function ReportQuery()
    {

        $Month = DB::table('requests AS R')

            ->orderBy('R.created_at', 'desc')

            ->select('R.Month')

            ->get()->unique('Month');

        $Year = DB::table('requests AS R')

            ->orderBy('R.created_at', 'desc')

            ->select('R.Year')

            ->get()->unique('Year');

        $data = [
            'Page'  => 'reports.Select',
            'Title' => 'Select the month and year to attach to the checkout report',
            'Desc'  => 'Checkout report time frame filter',

            'Month' => $Month,
            'Year'  => $Year,

        ];

        return view('index', $data);
    }

    public function ConvertToGet(Request $request)
    {
        $Month = $request->Month;
        $Year  = $request->Year;

        return redirect()->route('CheckoutReport',

            ['Month' => $Month,
                'Year'   => $Year,
            ]
        );
    }

    public function CheckIfResultsAreAvailable($Month, $Year)
    {
        $count = DB::table('requests AS R')

            ->where('R.Month', $Month)

            ->where('R.Year', $Year)

            ->where('R.ApprovalStatus', '=', 'approved')

            ->join('item_categories AS C', 'C.CID', '=', 'R.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'R.RequestedBy')

            ->select('R.*', 'C.Category', 'C.Unit', 'S.Name')

            ->orderBy('R.created_at', 'desc')

            ->count();

        return $count;
    }

    public function CheckoutReport($Month, $Year)
    {
        $count = $this->CheckIfResultsAreAvailable($Month, $Year);

        if ($count < 1) {

            return redirect()->back()->with('error_a', 'The selected query returned no results. Try again');

        }
        $App = DB::table('requests AS R')

            ->where('R.Month', $Month)

            ->where('R.Year', $Year)

            ->where('R.ApprovalStatus', '=', 'approved')

            ->join('item_categories AS C', 'C.CID', '=', 'R.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'R.RequestedBy')

            ->select('R.*', 'C.Category', 'C.Unit', 'S.Name')

            ->orderBy('R.created_at', 'desc')

            ->get();

        $data = [
            'Page'  => 'reports.CheckoutReport',
            'Title' => 'Inventory checkout report for the month ' . $Month . ' of the year ' . $Year,
            'Desc'  => ' Inventory checkout  report , filtered by timeframe',
            'App'   => $App,

        ];

        return view('index', $data);

    }

    public function Depleted()
    {
        $CategoryCount = ItemCategories::count();

        if ($CategoryCount < 1) {

            return redirect()->back()->with('error_a', 'No item categories detected, Please add them first and try again');

        }

        $Stock = DB::table('items AS I')

            ->where('I.Qty', '<=', 5)

            ->join('item_categories AS C', 'C.CID', '=', 'I.CID')

            ->join('staff AS S', 'S.EmpID', '=', 'I.EmpID')

            ->select('I.*', 'C.Category', 'C.Unit', 'S.Name')

            ->get();

        $Cats = ItemCategories::all();

        $data = [
            'Page'   => 'reports.Depleted',
            'Title'  => 'Stock Depletion Report',
            'Desc'   => 'Only items below a quantity of 5 in stock are shown',
            'Stock'  => $Stock,
            'Cats'   => $Cats,
            'BootOk' => 'true',
        ];

        return view('index', $data);
    }

    public function InvDocCats()
    {

        $Docs = DB::table('inventor_doc_categoaries')->get();
        $data = [
            'Page'  => 'InvDoc.MgtCat',
            'Title' => 'Manage Inventory Documentation Categories',
            'Desc'  => 'Inventory Documentation Settings',
            'Docs'  => $Docs,

        ];

        return view('index', $data);
    }

    public function NewInvDocCat(Request $request)
    {

        $validated = $this->validate($request, [
            'CatID'      => 'required|unique:inventor_doc_categoaries',
            'Category'   => 'required|unique:inventor_doc_categoaries',
            'created_at' => 'required',

        ]);

        DB::table('inventor_doc_categoaries')->insert(
            $request->except([
                '_token',

            ])
        );

        return redirect()->back()->with('status', 'New Inventory Category  Created successfully');
    }

    public function SelectDocCat()
    {
        $Cats = DB::table('inventor_doc_categoaries AS I')->get();
        $data = [
            'Page'  => 'InvDoc.SelectCat',
            'Title' => 'Select Inventory Document Category to document ',
            'Desc'  => 'Inventory Documentation, Category Selection',
            'Cats'  => $Cats,

        ];

        return view('index', $data);

    }

    public function DocSelected(Request $request)
    {
        $validated = $this->validate($request, [
            'CatID' => 'required',

        ]);

        $CatID = $request->CatID;

        Cache::put(Auth::user()->email, $CatID, 60 * 60);

        return redirect()->route('MgtInvFiles');

    }

    public function MgtInvFiles()
    {

        $key = Auth::user()->email;

        if (Cache::has($key)) {

            $CatID = Cache::get($key);

            $CatName = DB::table('inventor_doc_categoaries AS I')
                ->where('CatID', $CatID)
                ->first();

            $Docs = DB::table('inventory_docs AS I')

                ->where('I.CatID', $CatID)

                ->join('inventor_doc_categoaries AS C', 'C.CatID', '=', 'I.CatID')

                ->select('I.*', 'C.Category')

                ->get();

            $data = [
                'Page'    => 'InvDoc.MgtFiles',
                'Title'   => 'View all inventory files under the category  ' . $CatName->Category,
                'Desc'    => 'Inventory Documentation, Manage Files',
                'Docs'    => $Docs,
                'CatID'   => $CatID,
                'CatName' => $CatName->Category,
                'PDFJS'   => 'true',

            ];

            return view('index', $data);

        } else {

            echo "Cache adapter expired, go back to the select category page and repeat query";
        }

    }

    public function AddFile(Request $request)
    {
        $validated = $this->validate($request, [
            'CatID'       => 'required',
            'Url'         => 'required|mimes:pdf|max:9000',
            'DocID'       => 'required|unique:inventory_docs',
            'created_at'  => 'required',
            'Description' => 'required',
            'Title'       => 'required|unique:inventory_docs',

        ]);

        DB::table('inventory_docs')->insert(
            $request->except([
                '_token',

            ])
        );

        $PdfName = time() . '.' . $request->Url->extension();

        $request->Url->move(public_path('assets/docs'), $PdfName);

        DB::table('inventory_docs')->where('DocID', $request->DocID)->update([

            'Url' => 'assets/docs/' . $PdfName,
        ]);

        return redirect()->back()->with('status', 'Your new file has been uploaded successfully');

    }

    public function UpdateDocs(Request $request)
    {
        $validated = $this->validate($request, [
            'Description' => 'required',
            'Title'       => 'required',
            'TableName'   => 'required',
            'id'          => 'required',

        ]);

        $UploadUpdate = DB::table($request->TableName)
            ->where('id', $request->id)
            ->first();

        if ($request->hasfile('Url')) {

            unlink(public_path($UploadUpdate->Url));

            $PdfName = time() . '.' . $request->Url->extension();

            $request->Url->move(public_path('assets/docs'), $PdfName);

            DB::table('inventory_docs')->where('id', $request->id)->update([

                'Url' => 'assets/docs/' . $PdfName,
            ]);

        }

        DB::table($request->TableName)->where('id', $request->id)
            ->update(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                    'Url',

                ])
            );

        return redirect()->back()->with('status', 'Record updated successfully');
    }

    public function DeleteDoc($id)
    {
        $DelFile = DB::table('inventory_docs')->where('id', $id)->first();

        unlink(public_path($DelFile->Url));

        DB::table('inventory_docs')->where('id', $id)->delete();

        return redirect()->back()->with('status', 'Record deleted successfully');
    }
}
