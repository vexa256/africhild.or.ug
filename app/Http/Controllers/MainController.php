<?php

namespace App\Http\Controllers;

class MainController extends Controller
{

    public function dashboard()
    {
        $data = [

            //"Page"  => '',
            //"Title" => '',

        ];

        return view('index', $data);
    }

    public function UpdatePassword(Request $request)
    {

        $validated = $this->validate($request, [
            'id'       => 'required',
            'password' => 'required|confirmed',

        ]);
        \DB::table('users')->where('id', $request->id)->update([

            "password" => \Hash::make($request->password),
        ]);
    }
}
