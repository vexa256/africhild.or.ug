<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;

class TimeFix extends Controller
{
    public function FixTimestamps()
    {
        $MonthCount = DB::table('requests')->where('Month', '')
            ->orWhere('Month', null)
            ->count();

        $YearCount = DB::table('requests')->where('Year', '')
            ->orWhere('Year', null)
            ->count();

        if ($MonthCount > 0) {
            $Months = DB::table('requests')->where('Month', '')
                ->orWhere('Month', null)
                ->get();

            foreach ($Months as $M) {
                DB::table('requests')->where('id', $M->id)
                    ->update([
                        'Month' => date('M', strtotime($M->created_at)),
                    ]);
            }
        } elseif ($YearCount > 0) {

            $Years = DB::table('requests')->where('Year', '')
                ->orWhere('Year', null)
                ->get();

            foreach ($Years as $Y) {
                DB::table('requests')->where('id', $Y->id)
                    ->update([
                        'Year' => date('Y', strtotime($Y->created_at)),
                    ]);
            }
        }
    }
}
