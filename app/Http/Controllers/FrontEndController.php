<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InsertData;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FrontEndController extends Controller
{

    protected $Title = "The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children's wellbeing is realized for sustainable development.";

    protected $Desc = "The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing of children. Vision | We strive for an Africa where children's wellbeing is realized for sustainable development.";

    public function __construct()
    {
        if (\Schema::hasColumn('blogs', 'AfriChildBlogPost')) {

            $counter = DB::table('blogs')->where('AfriChildBlogPost', 'NULL')
                ->orWhere('AfriChildBlogPost', null)
                ->orWhere('AfriChildBlogPost', ' ')
                ->count();

            if ($counter > 0) {
                $this->SlugPostsTable();

            }

        } else {

            \Schema::table('blogs', function ($table) {
                $table->text('AfriChildBlogPost')->nullable();
            });

            $counter = DB::table('blogs')->where('AfriChildBlogPost', 'NULL')
                ->orWhere('AfriChildBlogPost', null)
                ->orWhere('AfriChildBlogPost', ' ')
                ->count();

            if ($counter > 0) {

                $this->SlugPostsTable();

            }

        }

    }

    public function SlugPostsTable(Type $var = null)
    {

        $blogs = DB::table('blogs')->where('AfriChildBlogPost', 'NULL')
            ->orWhere('AfriChildBlogPost', null)
            ->orWhere('AfriChildBlogPost', ' ')
            ->get();

        foreach ($blogs as $data) {

            DB::table('blogs')->where('id', $data->id)->update([

                "AfriChildBlogPost" => Str::slug($data->Title, '_'),
            ]);

        }

    }

    public function StoreImage($request, $TableName)
    {

        if ('policies' == $request->TableName) {

            $ThumbnailName = time() . '.' . $request->Thumbnail->extension();

            $request->Thumbnail->move(public_path('assets/docs'), $ThumbnailName);

            DB::table($TableName)->where('UID', $request->UID)->update([

                'Thumbnail' => 'assets/docs/' . $ThumbnailName,
            ]);
        }

        $ImageName = time() . '.' . $request->URL->extension();

        $request->URL->move(public_path('assets/docs'), $ImageName);

        DB::table($TableName)->where('UID', $request->UID)->update([

            'URL' => 'assets/docs/' . $ImageName,
        ]);
    }

    public function OurWork()
    {
        $partners = DB::table('partners')->get();
        $Sliders = DB::table('slider_settings')->get();
        $Areas = DB::table('program_areas')->latest()->get();
        $OurWorks = DB::table('our_works')->get();
        $SecTeam = DB::table('sec_teams')->get();
        $Policy = DB::table('policies')->get();
        $Directors = DB::table('directors')->get();
        $Gallery = DB::table('galleries')->get();
        $News = DB::table('news')->get();
        $Blog = DB::table('blogs')->latest()->get();
        $Founders = DB::table('founders')->get();
        $Assoc = DB::table('assocs')->get();
        $affs = DB::table('affs')->get();
        $Download = DB::table('downloads')->get();
        $data = [

            'Title' => $this->Title,
            'Desc' => $this->Desc,
            'Page' => "",
            'Sliders' => $Sliders,
            'Areas' => $Areas,
            'OurWorks' => $OurWorks,
            'SecTeam' => $SecTeam,
            'Policy' => $Policy,
            'Directors' => $Directors,
            'Photos' => $Gallery,
            'News' => $News,
            'Blog' => $Blog,
            'Founders' => $Founders,
            'Assoc' => $Assoc,
            'Aff' => $affs,
            'partners' => $partners,
            'Download' => $Download,

        ];

        return view('a_index.Work', $data);
    }

    public function BlogDetails()
    {
        $partners = DB::table('partners')->get();
        $Sliders = DB::table('slider_settings')->get();
        $Areas = DB::table('program_areas')->latest()->get();
        $OurWorks = DB::table('our_works')->get();
        $SecTeam = DB::table('sec_teams')->get();
        $Policy = DB::table('policies')->get();
        $Directors = DB::table('directors')->get();
        $Gallery = DB::table('galleries')->get();
        $News = DB::table('news')->get();
        $Blog = DB::table('blogs')->latest()->get();
        $Founders = DB::table('founders')->get();
        $Assoc = DB::table('assocs')->get();
        $affs = DB::table('affs')->get();
        $Download = DB::table('downloads')->get();
        $data = [

            'Title' => $this->Title,
            'Desc' => $this->Desc,
            'Page' => "",
            'Sliders' => $Sliders,
            'Areas' => $Areas,
            'OurWorks' => $OurWorks,
            'SecTeam' => $SecTeam,
            'Policy' => $Policy,
            'Directors' => $Directors,
            'Photos' => $Gallery,
            'News' => $News,
            'Blog' => $Blog,
            'Founders' => $Founders,
            'Assoc' => $Assoc,
            'Aff' => $affs,
            'partners' => $partners,
            'Download' => $Download,

        ];

        return view('a_index.Blog', $data);
    }

    public function Home()
    {
        $partners = DB::table('partners')->get();
        $Sliders = DB::table('slider_settings')->get();
        $Areas = DB::table('program_areas')->latest()->get();
        $OurWorks = DB::table('our_works')->get();
        $SecTeam = DB::table('sec_teams')->get();
        $Policy = DB::table('policies')->get();
        $Directors = DB::table('directors')->get();
        $Gallery = DB::table('galleries')->get();
        $News = DB::table('news')->get();
        $Blog = DB::table('blogs')->latest()->get();
        $Founders = DB::table('founders')->get();
        $Assoc = DB::table('assocs')->get();
        $affs = DB::table('affs')->get();
        $Download = DB::table('downloads')->get();
        $data = [

            'Title' => $this->Title,
            'Desc' => $this->Desc,
            'Page' => "",
            'Sliders' => $Sliders,
            'Areas' => $Areas,
            'OurWorks' => $OurWorks,
            'SecTeam' => $SecTeam,
            'Policy' => $Policy,
            'Directors' => $Directors,
            'Photos' => $Gallery,
            'News' => $News,
            'Blog' => $Blog,
            'Founders' => $Founders,
            'Assoc' => $Assoc,
            'Aff' => $affs,
            'partners' => $partners,
            'Download' => $Download,

        ];

        return view('a_index.index', $data);
    }

    public function MgtSlider()
    {
        $Sliders = DB::table('slider_settings')->get();

        $rem = [
            "SID",
            "id",
            "created_at",
            "updated_at",
            "URL",

        ];

        $data = [

            'Title' => "Manage Website Slider Settings",
            'Desc' => "Slider Settings",
            'Page' => "cms.SliderSettings",
            'Sliders' => $Sliders,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function NewSlider(Request $request)
    {
        $validated = $this->validate($request, [
            'SID' => 'required|unique:slider_settings',
            'CaptionOne' => 'required',
            'CaptionTwo' => 'required',
            'created_at' => 'required',
            'URL' => 'required',

        ]);

        DB::table('slider_settings')->insert(
            $request->except([
                '_token',

            ])
        );

        $ImageName = time() . '.' . $request->URL->extension();

        $request->URL->move(public_path('assets/docs'), $ImageName);

        DB::table('slider_settings')->where('SID', $request->SID)->update([

            'URL' => 'assets/docs/' . $ImageName,
        ]);

        return redirect()->back()->with('status', 'New slider created successfully');
    }

    public function ProgramAreas()
    {
        $Areas = DB::table('program_areas')->get();
        $rem = ['SID', 'id', 'UID', 'created_at', 'updated_at', 'URL'];

        $data = [

            'Title' => "Manage core program areas",
            'Desc' => "Website content Settings",
            'Page' => "cms.ProgramAreas",
            'Areas' => $Areas,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function OurWorks()
    {
        $OurWorks = DB::table('our_works')->get();
        $rem = ['SID', 'id', 'UID', 'created_at', 'updated_at', 'URL'];

        $data = [

            'Title' => "Manage our work section",
            'Desc' => "Website content Settings",
            'Page' => "cms.OurWork",
            'OurWorks' => $OurWorks,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function NewRecord(Request $request)
    {
        $InsertData = new InsertData;

        $InsertData->InsertData($request);

        return redirect()->back()->with('status', 'New record created successfully');

    }

    public function SecTeam()
    {
        $SecTeam = DB::table('sec_teams')->get();
        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed secretariat team members",
            'Desc' => "Website content Settings",
            'Page' => "cms.SecTeam",
            'SecTeam' => $SecTeam,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function PolicyBriefs()
    {
        $Policy = DB::table('policies')->get();

        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed policy briefings",
            'Desc' => "Website content Settings",
            'Page' => "cms.Policy",
            'PDFJS' => "true",
            'Policy' => $Policy,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function MgtResearch()
    {
        $rem = ["SID", "id", "created_at", "updated_at", "URL", "UID", "Thumbnail"];

        $Policy = DB::table('research')->get();

        $data = [

            'Title' => "Manage the displayed research publications",
            'Desc' => "Website content Settings",
            'Page' => "cms.Research",
            'PDFJS' => "true",
            'rem' => $rem,
            'Policy' => $Policy,

        ];

        return view('index', $data);
    }

    public function MgtReports()
    {
        $Policy = DB::table('reports')->get();

        $rem = ["SID", "id", "created_at", "updated_at", "URL", "UID", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed report publications",
            'Desc' => "Website content Settings",
            'Page' => "cms.Reports",
            'PDFJS' => "true",
            'Policy' => $Policy,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function Directors()
    {
        $Directors = DB::table('directors')->get();
        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed board of directors",
            'Desc' => "Website content Settings",
            'Page' => "cms.Directors",
            'Directors' => $Directors,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function Gallery()
    {
        $Gallery = DB::table('galleries')->get();
        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed photos in the gallery section",
            'Desc' => "Website content Settings",
            'Page' => "cms.Gallery",

            'Gallery' => $Gallery,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function jobs()
    {
        $jobs = DB::table('jobs')->get();

        $data = [

            'Title' => "Manage the displayed job openings in the career section",
            'Desc' => "Website content Settings",
            'Page' => "cms.Jobs",
            'PDFJS' => "true",
            'Jobs' => $jobs,

        ];

        return view('index', $data);
    }

    public function Download()
    {
        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $Download = DB::table('downloads')->get();

        $data = [

            'Title' => "Manage the displayed  download resources",
            'Desc' => "Website content Settings",
            'Page' => "cms.Download",
            'PDFJS' => "true",
            'Download' => $Download,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function News()
    {
        $News = DB::table('news')->get();

        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed  news posts",
            'Desc' => "Website content Settings",
            'Page' => "cms.News",
            'Edit' => "true",
            'News' => $News,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function Blog()
    {
        $Blog = DB::table('blogs')->get();

        $rem = ["UID", "id", "AfriChildBlogPost", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed  blog posts",
            'Desc' => "Website content Settings",
            'Page' => "cms.Blog",
            'Edit' => "true",
            'Blogs' => $Blog,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function Founders()
    {
        $Founders = DB::table('founders')->get();

        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed  founder records",
            'Desc' => "Website content Settings",
            'Page' => "cms.Founders",
            'Founders' => $Founders,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function Assoc()
    {

        $Assoc = DB::table('assocs')->get();

        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed  research associates records",
            'Desc' => "Website content Settings",
            'Page' => "cms.Assoc",
            'Assoc' => $Assoc,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function affiliate()
    {
        $affs = DB::table('affs')->get();

        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $data = [

            'Title' => "Manage the displayed  AfriChild affiliate records",
            'Desc' => "Website content Settings",
            'Page' => "cms.Aff",
            'affs' => $affs,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function Partners()
    {
        $rem = ["UID", "id", "created_at", "updated_at", "URL", "Thumbnail"];

        $partners = DB::table('partners')->get();

        $data = [

            'Title' => "Manage the displayed  promoting partner records",
            'Desc' => "Website content Settings",
            'Page' => "cms.Partners",
            'partners' => $partners,
            'rem' => $rem,

        ];

        return view('index', $data);
    }

    public function PolicyDetails()
    {
        $Policy = DB::table('policies')->get();

        $data = [

            'Title' => "Policy briefings published by AfriChild",
            'Desc' => "Policy Briefings | Informing Policy",
            'Page' => "cms.Partners",
            'Policy' => $Policy,

        ];

        return view('a_index.Policy', $data);
    }

    public function ResearchDetails()
    {

        $Research = DB::table('research')->get();

        $rem = [
            "SID",
            "id",
            "created_at",
            "updated_at",
            "URL",
            "Thumbnail",

        ];

        $data = [

            'Title' => "Child Centered Research published by AfriChild",
            'Desc' => "Research Publications",
            'Page' => "cms.Research",
            'Research' => $Research,
            'rem' => $rem,

        ];

        return view('a_index.Research', $data);
    }

    public function Reports(Type $var = null)
    {
        $Reports = DB::table('reports')->get();

        $data = [

            'Title' => "Reports published by AfriChild",
            'Desc' => "View and download reports",
            'Page' => "cms.Partners",
            'Reports' => $Reports,

        ];

        return view('a_index.Reports', $data);
    }

    public function PromotingPartners(Type $var = null)
    {
        $partners = DB::table('partners')->get();

        $data = [

            'Title' => "AfriChild's promoting Partners",
            'Desc' => "We greatly value partnerships",
            'Page' => "cms.Partners",
            'Partners' => $partners,

        ];

        return view('a_index.Partners', $data);
    }

    public function TheAfriChildCenterBlogPost($AfriChildBlogPost)
    {
        $partners = DB::table('partners')->get();
        $Sliders = DB::table('slider_settings')->get();
        $Areas = DB::table('program_areas')->latest()->get();
        $OurWorks = DB::table('our_works')->get();
        $SecTeam = DB::table('sec_teams')->get();
        $Policy = DB::table('policies')->get();
        $Directors = DB::table('directors')->get();
        $Gallery = DB::table('galleries')->get();
        $News = DB::table('news')->get();
        $Blog = DB::table('blogs')->where('AfriChildBlogPost', $AfriChildBlogPost)->first();
        $Other = DB::table('blogs')->latest()->take(5)->get();
        $Popular = DB::table('blogs')->orderBy('id', 'ASC')->get();
        $Founders = DB::table('founders')->get();
        $Assoc = DB::table('assocs')->get();
        $affs = DB::table('affs')->get();
        $Download = DB::table('downloads')->get();

        $data = [

            'Title' => $this->Title,
            'Desc' => $this->Desc,
            'Page' => "",
            'Sliders' => $Sliders,
            'Areas' => $Areas,
            'OurWorks' => $OurWorks,
            'SecTeam' => $SecTeam,
            'Policy' => $Policy,
            'Directors' => $Directors,
            'Photos' => $Gallery,
            'News' => $News,
            'Blog' => $Blog,
            'Popular' => $Popular,
            'Other' => $Other,
            'Founders' => $Founders,
            'Assoc' => $Assoc,
            'Aff' => $affs,
            'partners' => $partners,
            'Download' => $Download,
            // 'LoadDfree' =>"true",

        ];

        return view('a_details.BlogViewer', $data);

    }
}