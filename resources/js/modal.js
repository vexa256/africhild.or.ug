import { Modal } from "bootstrap";

document.addEventListener("DOMContentLoaded", () => {
    [].forEach.call(
        document.querySelectorAll(".RestockConfirm"),
        function (el) {
            el.addEventListener("click", function () {
                const TriggerModalW = $(this).data("triggermodal");
                console.log(TriggerModalW);

                window.myModal = new bootstrap.Modal(
                    document.getElementById(TriggerModalW)
                );

                Swal.fire({
                    title: "Are you sure?",
                    text: "You want to re-stock this item, This action is not reversible!!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes",
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.myModal.show();
                    }
                });
            });
        }
    );
}); //domcontent
