@include('a_header.header')
@include('a_header.topheader')
@include('a_header.nav')



<section class="inner-header"
    style="    padding-top: 60 px ; padding-bottom: 80 px ; position: relative; background: url({{ asset('img/sl1.jpg') }}) center center no-repeat; -webkit-background-size: cover; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2>AfriChild | Our Work</h2>
                <ul class="breadcumb">
                    <p>Some of our recent projects and partnerships</p>
                </ul>
                <span class="decor"><span
                        class="inner"></span></span>
            </div>
        </div>
    </div>
</section>

<section class="event-feature sec-padding bg-color-fa pb_60" id="CorePrograms">
    <div class="container">
        <div class="row">
            <div class="sec-title text-center" style="display:none">
                <h2>AfriChild | Our Work</h2>
                <p>To see all about our work click <a href="/OurWork"
                        style="color: red">here</a></p>
                <span class="decor">
                    <span class="inner"></span>
                </span>
            </div>

            @isset($OurWorks)

                @foreach ($OurWorks as $data)
                    <div class="rowss">
                        <div class="col-md-4">
                            <div class="event border-1px mb_30">
                                <div class="rodw">
                                    <div class="col-sm-12">
                                        <div class="event-thumb">
                                            <div class="thumb"
                                                style="padding-top: 20px; padding-bottom:20px">
                                                <img style="height:35vh"
                                                    class="full-width"
                                                    src="{{ asset($data->URL) }}"
                                                    alt="">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="event-content p_20">
                                            <h4 class="event-title"
                                                style="color: #df0a81; height: 60px ; overflow-y:scroll">
                                                {{ $data->Title }}
                                            </h4>
                                            <p class="textCut">
                                                {{ $data->Desc }}</p>
                                            <br>
                                            <a class="thm-btn btn-xs"
                                                data-toggle="modal"
                                                href="#assaaa{{ $data->id }}">Read
                                                More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset



        </div>

    </div>
</section>
@isset($OurWorks)
    @foreach ($OurWorks as $dataz)
        <div class="modal fade" id="assaaa{{ $dataz->id }}" tabindex="-1"
            role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg"
                role="document">
                <div class="modal-content">
                    <div class="modal-header"
                        style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            {{ $dataz->Title }}</h5>
                        <button type="button" class="close"
                            data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ $dataz->Desc }}
                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white"
                            type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset


<section class="bg-color-eee p_40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="clients-carousel owl-carousel owl-theme">
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/63.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/64.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/65.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/66.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/67.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/68.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/69.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/70.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('a_index.PageFooter')
