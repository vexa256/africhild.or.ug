@include('a_header.header')
@include('a_header.topheader')
@include('a_header.nav')

<section class="inner-header"
    style="    padding-top: 60 px ; padding-bottom: 80 px ; position: relative; background: url({{ asset('img/sl1.jpg') }}) center center no-repeat; -webkit-background-size: cover; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2>AfriChild | Report Publications</h2>
                <ul class="breadcumb">
                    <p>View and download our reports</p>
                </ul>
                <span class="decor"><span class="inner"></span></span>
            </div>
        </div>
    </div>
</section>
<section class="blog-home sec-padding">
    <div class="container">

        <div class="row">

            <div class="col-md-12" style="padding-bottom:40px; display: flex; align-items: center;
            justify-content: center;">
                <form style="display: flex">
                    <div style="display: flex;">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button"
                                id="dropdownMenu1" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="true">
                                Data Filter | Filter Report Publications | Titles
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                @isset($Reports)
                                    @foreach ($Reports as $dataz)
                                        <li>
                                            <a target="_blank"
                                                href="{{ asset($dataz->URL) }}"
                                                class="">
                                                {{ $dataz->Title }}
                                                {{ $dataz->Title2 }}</a>
                                        </li>
                                    @endforeach
                                @endisset


                            </ul>
                        </div>
                    </div>

                </form>
            </div>


            @isset($Reports)
                @foreach ($Reports as $data)
                    <div class="col-md-4">
                        <div class="single-team-member mb_60 shadow-lg">
                            <div class="img-box">
                                <img style="height:40vh" class="full-width"
                                    src="{{ asset($data->Thumbnail) }}" alt="">
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content"
                                            style="background-color: #c7e9fb; background-image: linear-gradient(315deg, #c7e9fb 0%, #e61d8c 74%); ">
                                            <ul>
                                                <li><a target="_blank"
                                                        href="{{ asset($dataz->URL) }}"
                                                        class=""><i
                                                            class="fas fa-binoculars"></i></a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5 style="font-weight: 700; color:gray">{{ $data->Title }}</h5>
                            <span>{{ $data->Title2 }}</span>
                            <p class="textCut">{{ $data->Desc }}
                            </p>
                            <a target="_blank" href="{{ asset($dataz->URL) }}"
                                class="thm-btn ">View
                                Document</a>
                        </div>
                    </div>
                @endforeach
            @endisset


        </div>
    </div>
</section>
@include('a_index.PageFooter')
