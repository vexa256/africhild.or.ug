@include('sections.mission')
@include('a_footer.footer')


<!-- The modal -->
<div class="modal fade" id="Aff" tabindex="-1" role="dialog"
    aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header"
                style="background-color: #df0a81; color:white">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLabel">AfriChild's Affiliates
                </h4>
            </div>
            <div class="modal-body"
                style="max-height: 400px ; overflow-y:scroll">

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Affiliate Name</th>

                        </tr>
                    </thead>
                    <tbody>

                        @isset($Aff)

                            @foreach ($Aff as $data)
                                <tr>

                                    <td>{{ $data->Name }}</td>
                                </tr>
                            @endforeach
                        @endisset


                    </tbody>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@include('a_scripts.scripts')
