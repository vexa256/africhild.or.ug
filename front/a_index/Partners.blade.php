@include('a_header.header')
@include('a_header.topheader')
@include('a_header.nav')

<section class="inner-header"
    style="    padding-top: 60 px ; padding-bottom: 80 px ; position: relative; background: url({{ asset('img/sl5.jpg') }}) center center no-repeat; -webkit-background-size: cover; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2>AfriChild | Promoting Partners</h2>
                <ul class="breadcumb">
                    <p>AfriChild values partnerships</p>
                </ul>
                <span class="decor"><span class="inner"></span></span>
            </div>
        </div>
    </div>
</section>
<section class="blog-home sec-padding">
    <div class="container">

        <div class="row">




            @isset($Partners)
                @foreach ($Partners as $data)
                    <div class="col-md-4">
                        <div class="single-team-member mb_60 shadow-lg">
                            <div class="img-box">
                                <img style="display: flex; height:27vh" class="full-width"
                                    src="{{ asset($data->URL) }}" alt="">

                            </div>
                            <h5 style="font-weight: 700; color:gray">{{ $data->Title }}</h5>
                            <span></span>
                            <p class="textCut">{{ $data->Desc }}
                            </p>
                            <a data-toggle="modal" href="#aaa{{ $data->id }}" class="thm-btn ">Read
                                More</a>
                        </div>
                    </div>
                @endforeach
            @endisset


        </div>
    </div>
</section>

@isset($Partners)
    @foreach ($Partners as $dataz)
        <div class="modal fade" id="aaa{{ $dataz->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $dataz->Title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ $dataz->Desc }}
                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white" type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset

@include('a_index.PageFooter')
