@include('a_header.header')
@include('a_header.topheader')
@include('a_header.nav')
@include('slider.slider')
@include('sections.mission')
@include('sections.CoreProgram')








@include('sections.ourwork')
@include('sections.Partners')

@include('sections.SecTeam')
@include('sections.mini')
@include('sections.policy')




@include('sections.Director')
@include('sections.Directors')
@include('sections.Founders')
@include('sections.Assoc')

@include('sections.Photos')












@include('sections.News')


@include('sections.Counters')

@include('sections.LatestBlogs')





<section class="bg-color-eee p_40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="clients-carousel owl-carousel owl-theme">
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/63.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/64.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/65.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/66.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/67.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/68.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/69.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important"
                                src="img/tim/70.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('sections.mission')
@include('a_footer.footer')

<!-- The modal -->
<div class="modal fade" id="Aff" tabindex="-1" role="dialog"
    aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header"
                style="background-color: #df0a81; color:white">
                <button type="button" class="close"
                    data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLabel">AfriChild's
                    Affiliates</h4>
            </div>
            <div class="modal-body"
                style="max-height: 400px ; overflow-y:scroll">

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Affiliate Name</th>

                        </tr>
                    </thead>
                    <tbody>

                        @isset($Aff)

                            @foreach ($Aff as $data)
                                <tr>

                                    <td>{{ $data->Name }}</td>
                                </tr>
                            @endforeach
                        @endisset


                    </tbody>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@include('a_scripts.scripts')
