@include('a_header.header')
@include('a_header.topheader')
@include('a_header.nav')



<section class="inner-header"
    style="    padding-top: 60 px ; padding-bottom: 80 px ; position: relative; background: url({{ asset('img/sl1.jpg') }}) center center no-repeat; -webkit-background-size: cover; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2>AfriChild Blog</h2>
                <ul class="breadcumb">
                    <p>Latest blog topics published by the AfriChild Center</p>
                </ul>
                <span class="decor"><span class="inner"></span></span>
            </div>
        </div>
    </div>
</section>

<section class="blog-home sec-padding">
    <div class="container">

        <div class="row">


            <div class="col-md-12" style="padding-bottom:40px; display: flex; align-items: center;
            justify-content: center;">
                <form style="display: flex">
                    <div style="display: flex;">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button"
                                id="dropdownMenu1" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="true">
                                Data Filter | Filter through our blog posts
                                <span class="caret"></span>
                            </button>
                            <ul style="max-height:200px; overflow-y:scroll"
                                class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                @isset($Blog)
                                    @foreach ($Blog as $dataz)
                                        <li style="max-height:200px; overflow-y:scroll">
                                            <a data-toggle="modal" class=""
                                                href="{{ route('TheAfriChildCenterBlogPost', ['AfriChildBlogPost' => $dataz->AfriChildBlogPost]) }}">{{ $dataz->Title }}</a>
                                        </li>
                                    @endforeach
                                @endisset


                            </ul>
                        </div>
                    </div>

                </form>
            </div>

            @isset($Blog)
                @foreach ($Blog as $data)
                    <div class="col-md-4 col-sm-12 sm-col5-center m-btms40">
                        <div class="single-blog-post pb-3">
                            <div class="img-box">
                                <img style="height:36vh; margin-top:50px !important"
                                    class="full-width " src="{{ asset($data->URL) }}"
                                    alt="">
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content overlay_me">
                                            <ul>
                                                <li>
                                                    <a
                                                        href="{{ route('TheAfriChildCenterBlogPost', ['AfriChildBlogPost' => $data->AfriChildBlogPost]) }}"><i
                                                            class="fa fa-link"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-box">
                                <div class="date-box">
                                    <div class="inner">
                                        <div class="date">
                                            <b>{!! date('d', strtotime($data->created_at)) !!}</b>
                                            {!! date('M', strtotime($data->created_at)) !!}
                                        </div>
                                        <div class="comment">
                                            {!! date('Y', strtotime($data->created_at)) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="content mb-3">
                                    <div href="#" style="height:80px ; overflow-y:scroll">
                                        <h3>{{ $data->Title }}</h3>
                                    </div>
                                    <p class="textCut">Click read more to see
                                        the full blog post</p>
                                    <a data-toggle="modal" class="thm-btn"
                                        href="{{ route('TheAfriChildCenterBlogPost', ['AfriChildBlogPost' => $data->AfriChildBlogPost]) }}">Read
                                        More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset

        </div>
    </div>
</section>







<section class="bg-color-eee p_40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="clients-carousel owl-carousel owl-theme">
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/63.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/64.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/65.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/66.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/67.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/68.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/69.png"
                                alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-box">
                            <img style="height:60px !important" src="img/tim/70.png"
                                alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('sections.mission')
@include('a_footer.footer')


<!-- The modal -->
<div class="modal fade" id="Aff" tabindex="-1" role="dialog"
    aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #df0a81; color:white">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLabel">AfriChild's
                    Affiliates</h4>
            </div>
            <div class="modal-body" style="max-height: 400px ; overflow-y:scroll">

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Affiliate Name</th>

                        </tr>
                    </thead>
                    <tbody>

                        @isset($Aff)

                            @foreach ($Aff as $data)
                                <tr>

                                    <td>{{ $data->Name }}</td>
                                </tr>
                            @endforeach
                        @endisset


                    </tbody>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@include('a_scripts.scripts')
