<section class="event-feature sec-padding bg-color-fa pb_60" id="CorePrograms">
    <div class="container">
        <div class="row">
            <div class="sec-title text-center">
                <h2>AfriChild's Core Program Areas</h2>

                <span class="decor">
                    <span class="inner"></span>
                </span>
            </div>

            @isset($Areas)

                @foreach ($Areas as $data)
                    <div class="col-sm-6 col-md-4">
                        <div class="event border-1px mb_30">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="event-thumb">
                                        <div class="thumb">
                                            <img style="height:40vh" class="full-width" src="{{ asset($data->URL) }}" alt="">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="event-content p_20">
                                        <h4 class="event-title" style="color: #df0a81">
                                            {{ $data->Title }}
                                        </h4>
                                        <p class="textCut">{{ $data->Desc }}</p>
                                        <br>
                                        <a class="thm-btn btn-xs" data-toggle="modal" href="#a{{ $data->id }}">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset



        </div>

    </div>
</section>
@isset($Areas)
    @foreach ($Areas as $dataz)
        <div class="modal fade" id="a{{ $dataz->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $dataz->Title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <p class="auto-g">
                            {{ $dataz->Desc }}
                        </p>

                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white" type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
