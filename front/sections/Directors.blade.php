<section class=" meet-Volunteer  " style="padding-top: 60px" id="DirectorsBoard">

    <div class="sec-title text-center ">
        <h2>Board of Directors</h2>

        <span class="decor"><span class="inner"></span></span>

    </div>
    <div class="container">

        <div class="clearfisx">
            <div class="team-carousel owl-carousel owl-theme">
                @isset($Directors)
                    @foreach ($Directors as $data)
                        <div class="item">
                            <div class="single-team-member">
                                <div class="">
                                    <div class="event border-1px mb_30">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="event-thumb">
                                                    <div class="thumb">
                                                        <img class="full-width" src="{{ asset($data->URL) }}" alt="">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="event-content p_20">
                                                    <h4 class="event-title" style="color: #df0a81">{{ $data->Name }} |
                                                        {{ $data->Title }}
                                                    </h4>
                                                    <p class="textCut">{{ $data->Desc }}</p>

                                                    <a class="thm-btn " data-toggle="modal"
                                                        href="#Directors{{ $data->id }}"> Read More </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset

            </div>
        </div>
    </div>
</section>
@isset($Directors)
    @foreach ($Directors as $dataz)
        <div class="modal fade" id="Directors{{ $dataz->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $dataz->Name }} | {{ $dataz->Title }}
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ $dataz->Desc }}
                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white" type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
