<section class="overlay-white  parallax-section  sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 promote-project text-center">
                <h3>Our Core Vision
                </h3>
                <div class="sec-title colored text-center">
                    <span class="decor">
                        <span class="inner"></span>
                    </span>
                </div>
                <h2>All about the African Child</h2>
                <p class="textCut" style="font-weight: 900"> We strive for an Africa where children's wellbeing is
                    realized for sustainable development.
                </p>


            </div>
        </div>
    </div>
</section>
