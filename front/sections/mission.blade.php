<section class="footer-call-to-action  sec-padding" id="AboutUs">
    <div class="container">
        <div class="row">
            <div class="col-md-9 sm-text-center">
                <h3>Building The next generation of child-
                    focused researchers</h3>
                <p>The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing
                    of children.</p>
            </div>
            <div class="col-md-3 text-right sm-text-center">
                <a href="#" class="thm-btn inverse m-tops15">Follow us on twitter</a>
            </div>
        </div>
    </div>
</section>

<section class="call-to-action">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="call-to-action-corner col-md-4 ">
                <div class="  single-call-to-action">
                    <div class="icon-box">
                        <div class="inner-box">
                            <i class="fa fa-globe fa-2x"></i>
                        </div>
                    </div>
                    <div class="content-box ">
                        <h3 id="">Our Vision </h3>
                        <p>We strive for an Africa where children's wellbeing is realized for sustainable development.
                        </p>

                    </div>
                </div>
            </div>
            <div class="call-to-action-center col-md-4">
                <div class="  single-call-to-action">
                    <div class="icon-box">
                        <div class="inner-box">
                            <i class="flaticon-social"></i>
                        </div>
                    </div>
                    <div class="content-box">
                        <h3>Our Mission
                        </h3>
                        <p>The AfriChild Centre exists to generate research that informs policy and practice for the
                            wellbeing of children.</p>

                    </div>
                </div>
            </div>
            <div class="call-to-action-corner col-md-4"
                style="background-image: url(img/call-to-action/right-box-bg.jpg);">
                <div class=" single-call-to-action">
                    <div class="icon-box">
                        <div class="inner-box">
                            <i class="flaticon-medical"></i>
                        </div>
                    </div>
                    <div class="content-box ">
                        <h3>Strategic Direction</h3>
                        </h3>
                        <p>View and download the AfriChild Strategic Plan</p>
                        <a data-doc="AfriChild Strategic Direction"
                            data-source="{{ asset('assets/docs/strategic-direction.pdf') }}" href="#PdfJS" href="#"
                            class="thm-btn  PdfViewer inverse">View</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
