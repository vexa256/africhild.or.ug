<section class="gallery-section " style="padding-top: 60px" id="Gallery">
    <div class="auto-container">
        <div class="sec-title text-center">
            <h2>Our Gallery</h2>
            <p>AfriChild in photos</p>
            <span class="decor"><span class="inner"></span></span>
        </div>
    </div>

    <div class="clearfix">

        @isset($Photos)
            @foreach ($Photos as $data)
                <div class="image-box">
                    <div class="inner-box">
                        <figure class="image"><a href="{{ asset($data->URL) }}" class="lightbox-image">
                                <img style="display: flex; max-height:200px; width:100%" src="{{ asset($data->URL) }}"
                                    alt="AfriChild-{{ $data->Title }}"></a></figure>
                        <a href="{{ asset($data->URL) }}" class="lightbox-image btn-zoom" title="Image Title Here"><span
                                class="icon fa fa-dot-circle-o"></span></a>
                    </div>
                </div>
            @endforeach
        @endisset

    </div>
</section>
