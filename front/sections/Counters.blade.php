<section class="footer-call-to-action" style="margin-top: 60px">
    <div class="container">
        <div class="row">
            <div class="col-md-9 sm-text-center">
                <h3>Children's Rights</h3>
                <p>AfriChild believes that all children in Africa
                    have a right to enjoy their full rights</p>
            </div>
            <div class="col-md-3 text-right sm-text-center">
                <a href="https://twitter.com/AfriChildCenter" target="_blank" class="thm-btn inverse m-tops15">Follow us
                    on twitter</a>
            </div>
        </div>
    </div>
</section>
