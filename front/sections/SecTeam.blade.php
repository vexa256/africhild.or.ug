<section class=" sec-padding meet-Volunteer bg-color-fa" id="SecTeam">

    <div class="sec-title text-center ">
        <h2>AfriChild | The Secretariat Team</h2>

        <span class="decor">
            <span class="inner"></span>
        </span>
    </div>
    <div class="container">

        <div class="clearfix m-topm50">
            <div class="team-carousel owl-carousel owl-theme">
                @isset($SecTeam)
                    @foreach ($SecTeam as $data)
                        <div class="item">
                            <div class="single-team-member"  style="">
                                <div class="img-box" style="">
                                    <img style="height:36vh" src="{{ asset($data->URL) }}" alt="">

                                </div>
                                <h3>{{ $data->Name }}</h3>
                                <span>{{ $data->Title }}</span>
                                <p class="textCut">{{ $data->Desc }}</p>
                                <a data-toggle="modal" href="#aaa{{ $data->id }}" class="thm-btn">Read More</a>
                            </div>
                        </div>
                    @endforeach
                @endisset


            </div>
        </div>
    </div>
</section>
@isset($SecTeam)
    @foreach ($SecTeam as $dataz)
        <div class="modal fade" id="aaa{{ $dataz->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $dataz->Title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ $dataz->Desc }}
                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white" type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
