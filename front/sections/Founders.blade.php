<section class=" meet-Volunteer  " style="padding-top: 60px" id="Founders">
    <div class="sec-title text-center ">
        <h2>Founding Partners</h2>

        <span class="decor"><span class="inner"></span></span>

    </div>
    <div class="container">

        <div class="clearfisx">
            <div class="team-carousel owl-carousel owl-theme">
                @isset($Founders)
                    @foreach ($Founders as $data)
                        <div class="item">
                            <div class="single-team-member">
                                <div class="">
                                    <div class="event border-1px mb_30">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="event-thumb">
                                                    <div class="thumb">
                                                        <img style="max-height: 200px" class="full-width"
                                                            src="{{ asset($data->URL) }}" alt="">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="event-content p_20">
                                                    <h4 style="font-size: 15px; color:red" class="event-title"
                                                        style="color: #df0a81">{{ $data->Name }}|
                                                        {{ $data->Title }}</h4>
                                                    <p class="textCut">
                                                        {{ $data->Desc }}
                                                    </p>

                                                    <a class="thm-btn " href="#"> Read More </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset


            </div>
        </div>
    </div>
</section>
