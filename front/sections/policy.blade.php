<section class=" meet-Volunteer  sec-padding" id="Policya">

    <div class="sec-title text-center ">
        <h2>Our Policy Briefings</h2>

        <span class="decor"><span class="inner"></span></span>

    </div>
    <div class="container">

        <div class="clearfix">
            <div class="team-carousel owl-carousel owl-theme">

                @isset($Policy)
                    @foreach ($Policy->take(4) as $data)
                        <div class="item">
                            <div class="single-team-member">
                                <div class="img-box">
                                    <img style="height:30vh"
                                        src="{{ asset($data->Thumbnail) }}" alt="">

                                </div>
                                <h3>{{ $data->Title }}</h3>
                                <span>{{ $data->Title2 }}</span>
                                <p class="textCut">{{ $data->Desc }}</p>
                                <a target="_blank" href="{{ asset($data->URL) }}"
                                    class="thm-btn ">View
                                    Document</a>
                            </div>
                        </div>
                    @endforeach
                @endisset



            </div>
        </div>
    </div>
</section>
