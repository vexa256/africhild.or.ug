<section style="padding-top:40px" class=" meet-Volunteer" id="NewsandEvents">

    <div class="sec-title text-center ">
        <h2>AfriChild News and Events</h2>
        <p>AfriChild's latest news events, Follow us on <a target="_blank" style="color: red"
                href="https://twitter.com/AfriChildCenter"> twitter</a>
            to stay updated</p>
        <span class="decor"><span class="inner"></span></span>

    </div>
    <div class="container">

        <div class="clearfisx">
            <div class="team-carousel owl-carousel owl-theme">


                @isset($News)
                    @foreach ($News as $data)
                        <div class="item">
                            <div class="single-team-member">
                                <div class="">
                                    <div class="single-blog-post">
                                        <div class="img-box">
                                            <img class="full-width" src="{{ asset($data->URL) }}" alt="">
                                            <div class="overlay">
                                                <div class="box">
                                                    <div class="content">
                                                        <ul>
                                                            <li><a data-toggle="modal" href="#Newss{{ $data->id }}"><i
                                                                        class="fa fa-link"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-box">

                                            <div class="content">
                                                <a class="textCut" data-toggle="modal"
                                                    href="#Newss{{ $data->id }}">
                                                    <h3>{{ $data->Title }}</h3>
                                                </a>
                                                <p class="textCut">
                                                    Click read more to see full story
                                                </p>
                                                <a class="thm-btn " data-toggle="modal"
                                                    href="#Newss{{ $data->id }}">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset


            </div>
        </div>
    </div>
</section>
@isset($News)
    @foreach ($News as $dataz)
        <div class="modal fade" id="Newss{{ $dataz->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $dataz->Title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <textarea class="tiny">
                                                                                                                                                            {!! $dataz->Desc !!}

                                                                                                                                                        </textarea>
                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white" type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
