<section class="event-feature  bg-color-fa">
    <div class="container">
        <div class="row">
            <div class="sec-title text-center">
                <h2>AfriChild | Our Work</h2>
                <p>To see all about our work click <a href="/OurWork"
                        style="color: red">here</a></p>
                <span class="decor">
                    <span class="inner"></span>
                </span>
            </div>
            @isset($OurWorks)
                @foreach ($OurWorks->take(2) as $data)
                    <div class="col-sm-12 col-md-6 col-lg-6  m-topm50">

                        <div class="featured-causes">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="thumb">
                                        <img style="height:65vh"
                                            class="full-width"
                                            src="{{ asset($data->URL) }}" alt="">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="causes-details">
                                        <h3>{{ $data->Title }}</h3>
                                        <p class="p-title"> The AfriChild
                                            Centre
                                        </p>
                                        <p class="textCut">
                                            {{ $data->Desc }}</p>
                                        <a class="thm-btn btn-xs"
                                            data-toggle="modal"
                                            href="#assaaa{{ $data->id }}">Read
                                            More</a>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset

        </div>
    </div>
</section>
@isset($OurWorks)
    @foreach ($OurWorks as $dataz)
        <div class="modal fade" id="assaaa{{ $dataz->id }}" tabindex="-1"
            role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true" style="z-index: 190000">
            <div class="modal-dialog modal-dialog-scrollable modal-lg"
                role="document">
                <div class="modal-content">
                    <div class="modal-header"
                        style="background-color: #df0a81; color:white">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            {{ $dataz->Title }}</h5>
                        <button type="button" class="close"
                            data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ $dataz->Desc }}
                    </div>
                    <div class="modal-footer">
                        <button style="background-color: #df0a81; color:white"
                            type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
