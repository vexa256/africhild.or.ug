{{-- <section class="blog-home sec-padding">
    <div class="container">
        <div class="sec-title text-center">
            <h2>AfriChild Blog</h2>
            <p>Latest blog topics published by the AfriChild Center</p>
            <span class="decor">
                <span class="inner"></span>
            </span>
        </div>
        <div class="row">

            @isset($Blog)
                @foreach ($Blog->take(3) as $data)
                    <div class="col-md-4 col-sm-12 sm-col5-center m-btms40">
                        <div class="single-blog-post">
                            <div class="img-box">
                                <img class="full-width" src="{{ asset($data->URL) }}" alt="">
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <ul>
                                                <li><a href="blog-details.html"><i class="fa fa-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-box">
                                <div class="date-box">
                                    <div class="inner">
                                        <div class="date">
                                            <b>{!! date('d', strtotime($data->created_at)) !!}</b>
                                            {!! date('M', strtotime($data->created_at)) !!}
                                        </div>
                                        <div class="comment">
                                            {!! date('Y', strtotime($data->created_at)) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="content">
                                    <a href="blog-details.html">
                                        <h3>{{ $data->Title }}</h3>
                                    </a>
                                    <p class="textCut">Click read more to see the full blog post</p>
                                    <a class="thm-btn" href="{{ route('BlogDetails') }}">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset

        </div>
    </div>
</section> --}}
