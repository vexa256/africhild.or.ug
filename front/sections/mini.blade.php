<section class="footer-call-to-action  sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-9 sm-text-center">
                <h3>Building The next generation of child-
                    focused researchers</h3>
                <p>The AfriChild Centre exists to generate research that informs policy and practice for the wellbeing
                    of children.</p>
            </div>
            <div class="col-md-3 text-right sm-text-center">
                <a href="#" class="thm-btn inverse m-tops15">Follow us on twitter</a>
            </div>
        </div>
    </div>
</section>
