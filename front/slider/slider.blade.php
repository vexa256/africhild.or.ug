<section class="rev_slider_wrapper" id="OurHome">
    <div id="slider1" class="rev_slider" data-version="5.0" id="">
        <ul>

            @isset($Sliders)

                @foreach ($Sliders as $data)
                    <li data-transition="parallaxvertical">
                        <img src="{{ asset($data->URL) }}" alt="" width="1920" height="705" data-bgposition="top center"
                            data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2">
                        <div class="tp-caption sfl tp-resizeme thm-banner-h1 blue-bg" data-x="left" data-hoffset="0"
                            data-y="top" data-voffset="249" data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="o:0" data-transform_out="o:0" data-start="500">
                            {{ $data->CaptionOne }}
                        </div>

                        <div class="tp-caption sfr tp-resizeme thm-banner-h1 heavy black-bg" data-x="left" data-hoffset="0"
                            data-y="top" data-voffset="318" data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
                            {{ $data->CaptionTwo }}
                        </div>

                        <div class="tp-caption sfl tp-resizeme" data-x="left" data-hoffset="0" data-y="top"
                            data-voffset="450" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0"
                            data-transform_out="o:0" data-start="2300">
                            <a href="#OurHome" class="thm-btn">Learn More</a>
                        </div>

                    </li>
                @endforeach

            @endisset






        </ul>
    </div>
</section>
