<section class="footer-bottom" style="background-color: #2C353C !important">
    <div align="center"><a class="twitter-timeline" data-height="600"
            data-width="1200" data-dnt="true"
            href="https://twitter.com/AfriChildCenter?ref_src=twsrc%5Etfw">Tweets
            by AfriChildCenter</a>
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8">
        </script>
    </div>
    <div class=" container text-center"
        style="background-color: #2C353C !important">
        <p><b>info@africhild.or.ug | +256 414 532 482 | Makerere University,
                Mary Stuart Rd, Kampala | All rights
                reserved by The AfriChild
                Center |
                {{ date('Y') }}</b>
        </p>
    </div>
</section>
