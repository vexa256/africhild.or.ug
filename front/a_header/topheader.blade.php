<header class="header"
    style="padding-top: 2px !important; padding-bottom:2px !important">
    <div class="container"
        style="padding-top: 2px !important; padding-bottom:2px !important">
        <div class="logo pull-left">
            <a href="https://www.africhild.or.ug/Home#OurHome">
                <img style="height: 70px !important" src="{{ asset('img/ft.png') }}"
                    alt="Awesome Image" />
            </a>
        </div>
        <div class="logo pull-left" style="margin-left:20px !important">
            <a target="_blank" href="https://www.mak.ac.ug/">
                <img style="height: 70px !important" src="{{ asset('img/Mak.png') }}"
                    alt="Awesome Image" />
            </a>
        </div>
        <div class="header-right-info pull-right clearfix">
            <div class="single-header-info">
                <div class="icon-box">
                    <div class="inner-box">
                        <i class="flaticon-interface-2"></i>
                    </div>
                </div>
                <div class="content">
                    <h3>EMAIL</h3>
                    <p><b>info@africhild.or.ug</b></p>
                </div>
            </div>
            <div class="single-header-info">
                <div class="icon-box">
                    <div class="inner-box">
                        <i class="flaticon-telephone"></i>
                    </div>
                </div>
                <div class="content">
                    <h3>Call Now</h3>
                    <p><b>+256 414 532 482 </b></p>
                </div>
            </div>
            <div class="single-header-info">
                <!-- Modal: donate now Starts -->
                <a class="thm-btn" target="_blank"
                    href="https://www.google.com/maps/place/AfriChild+Centre/@0.3319045,32.5668009,15z/data=!4m5!3m4!1s0x0:0xfcc7d746758c378e!8m2!3d0.3319017!4d32.5667731">Locate
                    Us</a>

                <!-- Modal: donate now Ends -->
            </div>
        </div>
    </div>
</header> <!-- /.header -->
