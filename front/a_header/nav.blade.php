<nav class="mainmenu-area stricky">
    <div class="container">
        <div class="navigation pull-left">
            <div class="nav-header">
                <ul>
                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="#OurHome" class="">Home</a>
                        @else
                            <a href="{{ route('Home') }}#OurHome" class="">Home</a>
                        @endif

                    </li>


                    <li class="dropdown">
                        <a href="#Testststs">Who we are</a>
                        <ul class="submenu">

                            @if (url()->current() == route('Home'))
                                <li> <a href="#AboutUs" class="">About</a></li>
                            @else
                                <li> <a href="{{ route('Home') }}#AboutUs" class="">About</a>
                                </li>
                            @endif


                            @if (url()->current() == route('Home'))
                                <li> <a href="{{ route('OurWork') }}" class="">Our Work</a></li>
                            @else
                                <li> <a href="{{ route('OurWork') }}" class="">Our Work</a>
                                </li>
                            @endif

                            @if (url()->current() == route('Home'))
                                <li><a href="#DirectorsBoard" class="">Board of Directors</a></li>
                            @else
                                <li> <a href="{{ route('Home') }}#DirectorsBoard" class="">Board of
                                        Directors</a>
                                </li>
                            @endif


                            @if (url()->current() == route('Home'))
                                <li><a href="#SecTeam" class="">The Secretariat</a></li>
                            @else
                                <li> <a href="{{ route('Home') }}#SecTeam" class="">The Secretariat</a>
                                </li>
                            @endif

                            <li>
                                @if (url()->current() == route('Home'))
                                    <a href="#CorePrograms" class="">Core Programs </a>
                                @else
                                    <a href="{{ route('Home') }}#CorePrograms" class="">Core
                                        Programs</a>
                                @endif

                            </li>

                            @if (url()->current() == route('Home'))
                                <li><a href="#Founders" class="">Founding Partners</a></li>
                            @else
                                <li> <a href="{{ route('Home') }}#Founders" class="">Founding
                                        Partners</a>
                                </li>
                            @endif

                            @if (url()->current() == route('Home'))
                                <li><a href="#Assoc" class="">Research Associates</a></li>
                            @else
                                <li> <a href="{{ route('Home') }}#Assoc" class="">Research
                                        Associates</a>
                                </li>
                            @endif

                            @if (url()->current() == route('Home'))
                                <li><a href="{{ route('PromotingPartners') }}" class="">Promoting
                                        Partners</a></li>
                            @else
                                <li> <a href="{{ route('PromotingPartners') }}" class="">Promoting
                                        Partners</a>
                                </li>
                            @endif

                            <li> <a data-toggle="modal" href="#Aff" class="">Affiliates</a>
                            </li>



                        </ul>
                    </li>

                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="{{ route('ResearchDetails') }}" class="">Research</a>
                        @else
                            <a href="{{ route('ResearchDetails') }}" class="">Research</a>
                        @endif

                    </li>

                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="{{ route('PolicyDetails') }}" class="">Policy</a>
                        @else
                            <a href="{{ route('PolicyDetails') }}" class="">Policy</a>
                        @endif

                    </li>

                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="{{ route('Reports') }}" class="">Reports</a>
                        @else
                            <a href="{{ route('Reports') }}" class="">Reports</a>
                        @endif

                    </li>
                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="#Gallery" class="">Gallery</a>
                        @else
                            <a href="{{ route('Home') }}#Gallery" class="">Gallery</a>
                        @endif

                    </li>

                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="{{ route('BlogDetails') }}" class="">Blog</a>
                        @else
                            <a href="{{ route('BlogDetails') }}" class="">Blog</a>
                        @endif

                    </li>

                    <li class="fix-me">
                        @if (url()->current() == route('Home'))
                            <a href="#NewsandEvents" class="">News</a>
                        @else
                            <a href="{{ route('Home') }}#NewsandEvents" class="">News</a>
                        @endif

                    </li>

                    <li class="dropdown">
                        <a href="#Testststs">Downloads</a>
                        <ul class="submenu">

                            @isset($Download)

                                @foreach ($Download as $data)
                                    <li><a target="_blank" href="{{ $data->URL }}"
                                            class="">{{ $data->Title }}</a></li>
                                @endforeach
                            @endisset








                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#Testststs">Staff Area</a>
                        <ul class="submenu">



                            <li> <a href="https://africhild.cloud/" class="">MIS</a>
                            </li>




                            <li> <a href="{{ route('dashboard') }}" class="">WEB-INV</a>
                            </li>



                        </ul>
                    </li>


                    <li class="fix-me "><a class="" href="https://hub.africhild.cloud/">Knowledge
                            Hub</a></li>
                </ul>
            </div>
            <div class="nav-footer">
                <button><i class="fa fa-bars"></i></button>
            </div>
        </div>

    </div>
</nav> <!-- /.mainmenu-area -->
