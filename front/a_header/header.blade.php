<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">


    <!-- responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @laravelPWA
    {!! SEO::generate(true) !!}


    <!-- master stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- responsive stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">


    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
        rel="stylesheet">

    <style>
        .modal-dialog {

            margin-top: 100px !important;

        }

        .dropdown:hover {
            background-color: white !important;
        }

        .fix-me:hover {
            background-color: white !important;
        }

        .textCut {
            overflow: hidden !important;
            text-overflow: ellipsis !important;
            display: -webkit-box !important;
            -webkit-line-clamp: 4 !important;
            /* number of lines to show */
            -webkit-box-orient: vertical !important;
        }

        .overlay_me {
            background-color: #c7e9fb !important;
            background-image: linear-gradient(315deg, #c7e9fb 0%, #e61d8c 74%) !important;
        }

        .call-to-action-center {
            background-color: #c7e9fb !important;
            background-image: linear-gradient(315deg, #c7e9fb 0%, #e61d8c 74%) !important;
        }

        .auto-g {

            word-wrap: break-word;
            white-space: pre-wrap;
        }

        .modal-title {

            font-weight: bold !important;
        }


        .images2 {
            display: flex;
            flex-wrap: wrap;

        }

        .imagewrapper2 {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;

        }

        .image2 {
            display: block;
            object-fit: cover;
            width: 100%;

            /* set to 'auto' in IE11 to avoid distortions */
        }

        .bg-color-fa {
            background-color: white !important;
        }

        .owl-controls {

            margin-top: 70px !important;


        }

        html {
            scroll-behavior: smooth;
        }


        .shadow-lg {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2) !important;
            transition: 0.3s !important;
            /*width: auto !important;*/
            padding: 10px;
            border-radius: 20px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #df0a81;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #df0a81;
        }

        .sec-padding {
            padding: 30px 0 !important;
        }

    </style>

</head>

<body class="bg-color-fa" id="fullpage">
