<!-- main jQuery -->
<script src="{{ url('js/jquery-1.11.1.min.js') }}"></script>
<!-- bootstrap -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- bx slider -->
<script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
<!-- owl carousel -->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<!-- validate -->
<script src="{{ asset('js/jquery-parallax.js') }}"></script>
<!-- validate -->
<script src="{{ asset('js/validate.js') }}"></script>
<!-- mixit up -->
<script src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
<!-- fancybox -->
<script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
<!-- easing -->
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<!-- circle progress -->
<script src="{{ asset('js/circle-progress.js') }}"></script>
<!-- appear js -->
<script src="{{ asset('js/jquery.appear.js') }}"></script>
<!-- count to -->
<script src="{{ asset('js/jquery.countTo.js') }}"></script>

<!-- isotope script -->
<script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
<!-- jQuery ui js -->
<script src="{{ asset('js/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- revolution scripts -->

<script src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

<!-- thm custom script -->


<script defer src="https://cdn.tiny.cloud/1/1nr3t3t5xeyg86kk7vb6p7u0d9eo1w4zd7dy14p1volsp9ed/tinymce/5/tinymce.min.js"
referrerpolicy="origin"></script>

<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
<script src="{{ asset('js/a_custom.js') }}"></script>


</body>



</html>
