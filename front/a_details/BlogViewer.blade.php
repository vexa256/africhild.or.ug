@include('a_header.header')
@include('a_header.topheader')
@include('a_header.nav')


<section class="inner-header"
    style="    padding-top: 60 px ; padding-bottom: 80 px ; position: relative; background: url({{ asset('img/sl2.png') }}) center center no-repeat; -webkit-background-size: cover; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2> AfriChild Blog Post</h2>
                <ul class="breadcumb">
                    <strong>
                        <p>{{ $Blog->Title }}</p>
                    </strong>
                </ul>
                <span class="decor"><span class="inner"></span></span>
            </div>
        </div>
    </div>
</section>


<section class="blog-home sec-padding blog-page blog-details">
    <div class="container">
        <div class="row">
            <div class="col-md-8 pull-left">
                <div class="single-blog-post">
                    <div class="img-box">
                        {{-- <img src="img/blog-page/2.jpg" alt=""> --}}
                    </div>
                    <div class="content-box">
                        <div class="date-box">
                            <div class="inner">
                                <div class="date">
                                    <b>24</b>
                                    apr
                                </div>
                                <div class="comment">
                                    <i class="flaticon-interface-1"></i> 8
                                </div>
                            </div>
                        </div>
                        <div class="content" style="height:2px; overflow:hidden">
                            <a href="#">
                                <h3>{{ $Blog->Title }}</h3>
                            </a>

                            <div class="card-body shadow-lg"
                                style="height: 700px; overflow-y: scroll">
                                {!! $Blog->Desc !!}
                            </div>




                            <div class="bottom-box clearfix">

                            </div>
                        </div>
                    </div>
                </div>




            </div>
            <div class="col-md-4 pull-right">
                <div class="side-bar-widget">

                    <div class="single-sidebar-widget popular-post">
                        <h3 class="title">Latest Blog Posts</h3>
                        <ul>


                            @isset($Other)
                                @foreach ($Other->shuffle() as $d)
                                    <li>
                                        <div class="img-box">
                                            <div class="inner-box">
                                                <img src="{{ asset($d->URL) }}"
                                                    alt="The AfriChild Center">
                                            </div>
                                        </div>
                                        <div class="content-box">
                                            <a
                                                href="{{ route('TheAfriChildCenterBlogPost', ['AfriChildBlogPost' => $d->AfriChildBlogPost]) }}">
                                                <h4>{{ Str::limit($d->Title, 100, $end = '.......') }}
                                                </h4>
                                            </a>
                                            <span>{!! date('F j, Y', strtotime($d->created_at)) !!}</span>
                                        </div>
                                    </li>
                                @endforeach
                            @endisset


                        </ul>
                    </div>





                </div>
            </div>
            <div class="col-md-6 ">
                <div class="side-bar-widget">

                    <div class="single-sidebar-widget popular-post">
                        <h3 class="title">General Posts</h3>
                        <ul>
                            @isset($Popular)
                                @foreach ($Popular->shuffle()->slice(0, 4) as $data)
                                    <li>
                                        <div class="img-box">
                                            <div class="inner-box">
                                                <img src="{{ asset($data->URL) }}"
                                                    alt="The AfriChild Center">
                                            </div>
                                        </div>
                                        <div class="content-box">
                                            <a
                                                href="{{ route('TheAfriChildCenterBlogPost', ['AfriChildBlogPost' => $data->AfriChildBlogPost]) }}">
                                                <h4>{{ Str::limit($data->Title, 100, $end = '.......') }}
                                                </h4>
                                            </a>
                                            <span>{!! date('F j, Y', strtotime($data->created_at)) !!}</span>
                                        </div>
                                    </li>
                                @endforeach
                            @endisset

                        </ul>
                    </div>





                </div>
            </div>
            <div class="col-md-6 pull-right">
                <div class="side-bar-widget">

                    <div class="single-sidebar-widget popular-post">
                        <h3 class="title">Popular Posts</h3>
                        <ul>
                            @isset($Popular)
                                @foreach ($Popular->shuffle()->slice(0, 4) as $datas)
                                    <li>
                                        <div class="img-box">
                                            <div class="inner-box">
                                                <img src="{{ asset($datas->URL) }}"
                                                    alt="The AfriChild Center">
                                            </div>
                                        </div>
                                        <div class="content-box">
                                            <a
                                                href="{{ route('TheAfriChildCenterBlogPost', ['AfriChildBlogPost' => $datas->AfriChildBlogPost]) }}">
                                                <h4 class="cut-text">
                                                    {{ Str::limit($datas->Title, 100, $end = '.......') }}
                                                </h4>
                                            </a>
                                            <span>{!! date('F j, Y', strtotime($datas->created_at)) !!}</span>
                                        </div>
                                    </li>
                                @endforeach
                            @endisset

                        </ul>
                    </div>





                </div>
            </div>
        </div>
    </div>
</section>

@include('a_index.PageFooter')
