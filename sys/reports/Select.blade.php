<div class="card shadow-lg">
    <div class="card-header">
        <h2 class="card-title">
            {{ $Title }}
        </h2>
    </div>

    <div class="card-body">
        <form action="{{ route('ConvertToGet') }} " method="POST">
            @csrf
            <div class="row">

                <div class="col-md-6">
                    <div class="mb-3">
                        <div class="form-label">Select Month</div>
                        <select name="Month" class="form-select flexselect form-control">
                            <option value=""></option>
                            @isset($Month)
                                @foreach ($Month as $data)
                                    <option value="{{ $data->Month }}">
                                        {{ $data->Month }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-3">
                        <div class="form-label">Select Year</div>
                        <select name="Year" class="form-select flexselect form-control">
                            <option value=""></option>
                            @isset($Year)
                                @foreach ($Year as $datas)
                                    <option value="{{ $datas->Year }}">
                                        {{ $datas->Year }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                </div>

                <div class="col-12">
                    <div class="mb-3 mt-3 float-end">
                        <button type="submit" class="btn btn-danger">
                            Generate Report
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
