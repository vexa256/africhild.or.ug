<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Title }}
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Item </th>
                        <th>Description </th>
                        <th>Units </th>
                        <th>Qty Available </th>
                        <th>Last Modified By </th>
                        <th>Re-stock </th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Stock)
                        @foreach ($Stock as $data)
                            <tr>
                                <td>{{ $data->Item }}</td>
                                <td>{{ $data->BriefDesc }}</td>
                                <td>{{ $data->Unit }}</td>
                                <td>{{ $data->Qty }}</td>
                                <td>{{ $data->Name }}</td>


                                <td>

                                    <a data-bs-toggle="modal" href="#Restock{{ $data->id }}"
                                        class="btn RestockConfirm btn-dark btn-sm btn-shadow">
                                        <i class="fas  me-1 fa-plus" aria-hidden="true"></i>
                                        Re-stock </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>
@include('reports.Restock')
