<div class="card">
    <div class="card-header">
        <h3 class="card-title">

            {{ $Desc }}

        </h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Item </th>
                        <th>Description </th>
                        <th>Units </th>
                        <th>Qty Requested </th>
                        <th>Approval Status </th>
                        <th>Requested By </th>


                    </tr>
                </thead>
                <tbody>

                    @isset($App)
                        @foreach ($App as $data)
                            <tr>
                                <td>{{ $data->Item }}</td>
                                <td>{{ $data->BriefDesc }}</td>
                                <td>{{ $data->Unit }}</td>
                                <td>{{ $data->QtyRequested }}</td>
                                <td>{{ $data->ApprovalStatus }}</td>
                                <td>{{ $data->Name }}</td>


                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>
</div>
