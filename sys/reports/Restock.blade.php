@isset($Stock)
    @foreach ($Stock as $data)
        <div class="modal modal-blur fade" id="Restock{{ $data->id }}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Restock the selected item

                            <span class="text-danger">

                                {{ $data->Item }}

                            </span>
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('UpdateLogic') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label class="form-label">Quantity to restock</label>
                                <input required type="text" class="form-control IntOnlyNow" name="Qty">
                            </div>

                            <input required required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">

                            <input required type="hidden" name="EmpID" value="{{ Auth::user()->EmpID }}">

                            <input required type="hidden" name="TableName" value="items">

                            <input required type="hidden" name="id" value="{{ $data->id }}">
                            <div class="modal-footer">
                                <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                                    Cancel
                                </a>
                                <button type="submit" class="btn btn-orange btn-pill ms-auto">
                                    <i class="fas me-1 fa-check" aria-hidden="true"></i>
                                    Save Changes
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
