<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewCat" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New Category
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Title }}
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Category </th>
                        <th>Measurement Unit(s)</th>
                        <th>Update</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>

                    @isset($Cats)
                        @foreach ($Cats as $data)
                            <tr>
                                <td>{{ $data->Category }}</td>
                                <td>{{ $data->Unit }}</td>

                                <td>

                                    <a data-bs-toggle="modal" href="#UpdateCat{{ $data->id }}"
                                        class="btn  btn-dark btn-sm btn-shadow">
                                        <i class="fas  me-1 fa-edit" aria-hidden="true"></i>
                                        Update </a>
                                </td>
                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'item_categories']) }}"
                                        data-msg="You want to delete this Category. This action is not reversible" href="#"
                                        class="btn deleteConfirm btn-orange btn-sm btn-shadow"> <i
                                            class="fas me-1  fa-trash" aria-hidden="true"></i> Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('categories.NewCat')

@include('categories.UpdateCat')
