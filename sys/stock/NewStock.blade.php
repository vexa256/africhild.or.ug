<div class="modal modal-blur fade" id="NewStock" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create new inventory item record</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="{{ route('NewItem') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label">Item</label>
                        <input required type="text" class="form-control" name="Item">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Quantity (Integers Only)</label>
                        <input required type="text" class="form-control IntOnlyNow" name="Qty">
                    </div>


                    <div class="mb-3">
                        <div class="form-label">Category</div>
                        <select name="CID" class="form-select flexselect form-control">
                            <option value=""></option>
                            @isset($Cats)
                                @foreach ($Cats as $data)
                                    <option value="{{ $data->CID }}">
                                        {{ $data->Category }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Very Brief Description</label>
                        <input required type="text" class="form-control " name="BriefDesc">
                    </div>

                    <input type="hidden" name="WarningAmount" value="5">
                    <input required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">

                    <input type="hidden" name="EmpID" value="{{ Auth::user()->EmpID }}">




                    {!! Form::hidden($name = 'IID', $value = \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')), [($options = null)]) !!}




                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
