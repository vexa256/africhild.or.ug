<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewStock" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New Item
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Title }}
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Item </th>
                        <th>Description </th>
                        <th>Units </th>
                        <th>Qty Available </th>
                        <th>Last Modified By </th>

                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>

                    @isset($Stock)
                        @foreach ($Stock as $data)
                            <tr>
                                <td>{{ $data->Item }}</td>
                                <td>{{ $data->BriefDesc }}</td>
                                <td>{{ $data->Unit }}</td>
                                <td>{{ $data->Qty }}</td>
                                <td>{{ $data->Name }}</td>

                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'items']) }}"
                                        data-msg="You want to delete this inventory item. This action is not reversible"
                                        href="#" class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('stock.NewStock')

@include('stock.Update')
