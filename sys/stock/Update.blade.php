@isset($Stock)
    @foreach ($Stock as $data)
        <div class="modal modal-blur fade" id="UpdateStock{{ $data->id }}" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">

                            Update the selected inventory item <span class="text-danger">
                                {{ $data->Item }}
                            </span>


                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('UpdateLogic') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label class="form-label">Item</label>
                                <input value="{{ $data->Item }}" required type="text" class="form-control" name="Item">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Quantity (Integers Only)</label>
                                <input value="{{ $data->Qty }}" required type="text" class="form-control IntOnlyNow"
                                    name="Qty">
                            </div>


                            <div class="mb-3">
                                <div class="form-label">Category</div>
                                <select name="CID" class="form-select flexselect form-control">
                                    <option value="{{ $data->CID }}">
                                        {{ $data->Category }}</option>
                                    @isset($Cats)
                                        @foreach ($Cats as $c)
                                            <option value="{{ $c->CID }}">
                                                {{ $c->Category }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Very Brief Description</label>
                                <input value="{{ $data->BriefDesc }}" required type="text" class="form-control "
                                    name="BriefDesc">
                            </div>




                            <input type="hidden" name="EmpID" value="{{ Auth::user()->EmpID }}">

                            <input type="hidden" name="id" value="{{ $data->id }}">

                            <input type="hidden" name="TableName" value="items">




                            <div class="modal-footer">
                                <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                                    Cancel
                                </a>
                                <button type="submit" class="btn btn-orange btn-pill ms-auto">
                                    <i class="fas me-1 fa-check" aria-hidden="true"></i>
                                    Save Changes
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
