<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#Req" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New Request
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Title }}
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Item </th>
                        <th>Description </th>
                        <th>Units </th>
                        <th>Qty Requested </th>
                        <th>Approval Status </th>
                        <th>Requested By </th>

                        <th>Update</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>

                    @isset($Req)
                        @foreach ($Req as $data)
                            <tr>
                                <td>{{ $data->Item }}</td>
                                <td>{{ $data->BriefDesc }}</td>
                                <td>{{ $data->Unit }}</td>
                                <td>{{ $data->QtyRequested }}</td>
                                <td @if ($data->ApprovalStatus == 'approved')

                                    class="bg-success "
                        @endif

                        >{{ $data->ApprovalStatus }}</td>
                        <td>{{ $data->Name }}</td>


                        <td>
                            Not allowed
                            <a data-bs-toggle="modal" href="#ReqUpdate{{ $data->id }}"
                                class="btn d-none btn-dark btn-sm btn-shadow">
                                <i class="fas  me-1 fa-edit" aria-hidden="true"></i>
                                Update </a>
                        </td>
                        <td>
                            @if ($data->ApprovalStatus == 'pending')
                                <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'requests']) }}"
                                    data-msg="You want to delete this inventory item request . This action is not reversible"
                                    href="#" class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                    <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                    Delete </a>
                            @endif

                        </td>
                        </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('request.MakeReq')

@include('request.Update')
