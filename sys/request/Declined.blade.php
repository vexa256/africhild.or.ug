<div class="modal modal-blur fade" id="Declined" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-fullscreen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Declined Consumable Requests Log</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">


                <table style="width: 100% !important" class=" mytable table  table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th>Item </th>
                            <th>Description </th>
                            <th>Units </th>
                            <th>Qty Requested </th>
                            <th>Approval Status </th>
                            <th>Requested By </th>
                            <th>Reason Declined</th>



                        </tr>
                    </thead>
                    <tbody>

                        @isset($Dec)
                            @foreach ($Dec as $data)
                                <tr>
                                    <td>{{ $data->Item }}</td>
                                    <td>{{ $data->BriefDesc }}</td>
                                    <td>{{ $data->Unit }}</td>
                                    <td>{{ $data->QtyRequested }}</td>
                                    <td class="bg-warning">{{ $data->ApprovalStatus }}</td>
                                    <td>{{ $data->Name }}</td>
                                    <td class="bg-danger text-light">{{ $data->DeclineReason }}</td>



                                </tr>
                            @endforeach
                        @endisset


                    </tbody>

                </table>

            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                    Cancel
                </a>

            </div>
        </div>
    </div>
</div>
