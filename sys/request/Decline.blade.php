@isset($Req)
    @foreach ($Req as $data)
        <div class="modal modal-blur fade" id="Decline{{ $data->id }}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Decline the selected consumable request

                            from the staff member


                            <span class="text-danger">
                                {{ $data->Name }}
                            </span>

                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <form action="{{ route('UpdateLogic') }}" method="POST">

                            @csrf

                            <div class="mb-3">
                                <label class="form-label">Reason for declining request

                                </label>
                                <input type="text" class="form-control" name="DeclineReason">
                            </div>

                            <input type="hidden" name="ApprovalStatus" value="declined">

                            <input type="hidden" name="TableName" value="requests">

                            <input type="hidden" name="id" value="{{ $data->id }}">

                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                        </a>

                        <button type="submit" class="btn btn-danger">
                            Save Changes
                        </button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
