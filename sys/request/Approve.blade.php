<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#Approved" data-bs-toggle="modal" class="btn btn-danger  btn-sm">
                <i class="fa fa-check me-1" aria-hidden="true"></i>

                Approved Requests
            </a>

            <a href="#Declined" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-check me-1" aria-hidden="true"></i>

                Declined Requests
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Title }}
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Item </th>
                        <th>Description </th>
                        <th>Units </th>
                        <th>Qty Requested </th>
                        <th>Approval Status </th>
                        <th>Requested By </th>

                        <th>Approve</th>
                        <th>Decline</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Req)
                        @foreach ($Req as $data)
                            <tr>
                                <td>{{ $data->Item }}</td>
                                <td>{{ $data->BriefDesc }}</td>
                                <td>{{ $data->Unit }}</td>
                                <td>{{ $data->QtyRequested }}</td>
                                <td>{{ $data->ApprovalStatus }}</td>
                                <td>{{ $data->Name }}</td>


                                <td>
                                    <a data-route="{{ route('Approve', ['id' => $data->id]) }}"
                                        data-msg="You want to approve this consumable request . This action is not reversible"
                                        href="#" class="btn deleteConfirm2 btn-danger btn-sm btn-shadow">
                                        <i class="fas  me-1 fa-edit" aria-hidden="true"></i>
                                        Approve </a>
                                </td>
                                <td>

                                    <a data-bs-toggle="modal" href="#Decline{{ $data->id }}"
                                        class="btn  btn-dark btn-sm btn-shadow">
                                        <i class="fas  me-1 fa-edit" aria-hidden="true"></i>
                                        Decline </a>
                                </td>

                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('request.Approved')
@include('request.Declined')
@include('request.Decline')
