@isset($Req)
    @foreach ($Req as $d)
        <div class="modal modal-blur fade" id="ReqUpdate{{ $d->id }}" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update the selected item request

                            <span class="text-danger">
                                {{ $d->Item }}
                            </span>

                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('UpdateLogic') }}" method="POST">
                            @csrf

                            <input required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">
                            <div class="mb-3">
                                <div class="form-label">Consumable</div>
                                <select name="IID" class="form-select flexselect form-control">
                                    <option value="{{ $d->IID }}">{{ $d->Item }}</option>
                                    @isset($Items)
                                        @foreach ($Items as $d)
                                            <option value="{{ $d->IID }}">
                                                {{ $d->Item }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>


                            <div class="mb-3">
                                <label class="form-label">Brief Request Description</label>
                                <input value="{{ $d->BriefDesc }}" required type="text" class="form-control "
                                    name="BriefDesc">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Quantity Requested (Integers Only)</label>
                                <input value="" required type="text" class="form-control IntOnlyNow" name="QtyRequested">
                            </div>


                            <input type="hidden" name="id" value="{{ $d->id }}">

                            <input type="hidden" name="TableName" value="requests">


                            <div class="modal-footer">
                                <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                                    Cancel
                                </a>
                                <button type="submit" class="btn btn-orange btn-pill ms-auto">
                                    <i class="fas me-1 fa-check" aria-hidden="true"></i>
                                    Save Changes
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
