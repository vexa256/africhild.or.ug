<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewSlider" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New download resource
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            @isset($Title)
                {{ $Desc }} | Only PDF files are supported
            @endisset
        </h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Title </th>
                        <th>PDF </th>
                        <th>Preview </th>
                        <th>Edit </th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Download)
                        @foreach ($Download as $data)
                            <tr>
                                <td>
                                    @if ($data->Title == '')
                                        N/A
                                    @else
                                        {{ $data->Title }}
                                    @endif
                                </td>

                                <td>
                                    <a data-doc="  {{ $data->Title }} " data-source="{{ asset($data->URL) }}"
                                        data-bs-toggle="modal" href="#PdfJS" class="btn btn-sm  PdfViewer btn-primary"> <i
                                            class="fas me-1  fa-file-pdf" aria-hidden="true"></i> View
                                    </a>
                                </td>

                                <td><a href="{{ url('/') }}" class="btn btn-sm btn-dark">
                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> Preview
                                    </a></td>


                                {{ UpdateCms($data) }}
                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'jobs']) }}"
                                        data-msg="You want to delete this record. This action is not reversible" href="#"
                                        class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('cms.NewDownload')
@include('cms.Pdf')



@isset($Download)
    @foreach ($Download as $data)
        <form novalidate action="{{ route('CMSUpdate') }}" class="" method="POST"
            enctype="multipart/form-data">
            @csrf

            <div class="row">
                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="downloads">


                {{ RunUpdateModal($ModalID = $data->id,$Extra ='<div class="mb-10"> <label for="" class="required form-label">Thumbnail</label> <input required type="file" class="form-control form-control-solid" name="URL" />',$csrf = null,$Title = 'Update the selected  record',$RecordID = $data->id,$col = '12',$te = '12',$TableName = 'downloads') }}
            </div>
        </form>
    @endforeach
@endisset
