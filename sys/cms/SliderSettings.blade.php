<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewSlider" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New Slider
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            @isset($Title)
                {{ $Desc }}
            @endisset
        </h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Slider Title </th>
                        <th>Image </th>
                        <th>Button Link </th>
                        <th>Caption One </th>
                        <th>Caption Two </th>
                        <th>Preview </th>
                        <th>Edit </th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Sliders)
                        @foreach ($Sliders as $data)
                            <tr>
                                <td>
                                    @if ($data->Title == '')
                                        N/A
                                    @else
                                        {{ $data->Title }}
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-dark btn-sm" data-lightbox="image-1" data-title="My caption"
                                        href="{{ asset($data->URL) }}">

                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> View
                                    </a>

                                </td>
                                <td>{{ $data->BtnLinkUrl }}</td>
                                <td>{{ $data->CaptionOne }}</td>
                                <td>{{ $data->CaptionTwo }}</td>
                                <td><a href="{{ url('/') }}" class="btn btn-sm btn-dark">
                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> Preview
                                    </a></td>

                                <td>
                                    <a data-bs-toggle="modal" href="#Update{{ $data->id }}" class="btn btn-sm btn-dark">
                                        <i class="fas fa-edit" aria-hidden="true"></i>
                                    </a>
                                </td>

                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'slider_settings']) }}"
                                        data-msg="You want to delete this slider image. This action is not reversible"
                                        href="#" class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('cms.NewSlider')



@isset($Sliders)
    @foreach ($Sliders as $data)
        <form action="{{ route('CMSUpdate') }}" class="" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="slider_settings">


                {{ RunUpdateModal($ModalID = $data->id,$Extra ='<div class="mb-10"> <label for="" class="required form-label">Slider Image</label> <input required type="file" class="form-control form-control-solid" name="URL" />',$csrf = null,$Title = 'Update the selected slider record',$RecordID = $data->id,$col = '12',$te = '12',$TableName = 'slider_settings') }}
            </div>
        </form>
    @endforeach
@endisset
