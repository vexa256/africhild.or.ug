<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewSlider" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                Add blog post
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            @isset($Title)
                {{ $Desc }} | Only Portrait images are supported
            @endisset
        </h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Title </th>
                        <th>Content </th>
                        <th>Thumbnail </th>
                        <th>Preview </th>
                        <th>Edit </th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Blogs)
                        @foreach ($Blogs as $data)
                            <tr>
                                <td> {{ $data->Title }} </td>
                                <td>

                                    <a class="btn btn-dark btn-sm" data-bs-toggle="modal"
                                        href="#News{{ $data->id }}">

                                        <i class="fas me-1 fa-binoculars"
                                            aria-hidden="true"></i> View
                                    </a>



                                </td>
                                <td>

                                    <a class="btn btn-dark btn-sm" data-lightbox="image-1"
                                        data-title="My caption"
                                        href="{{ asset($data->URL) }}">

                                        <i class="fas me-1 fa-binoculars"
                                            aria-hidden="true"></i> View
                                    </a>



                                </td>

                                <td><a href="{{ url('/') }}" class="btn btn-sm btn-dark">
                                        <i class="fas me-1 fa-binoculars"
                                            aria-hidden="true"></i> Preview
                                    </a></td>

                                {{ UpdateCms($data) }}

                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'blogs']) }}"
                                        data-msg="You want to delete this record. This action is not reversible"
                                        href="#"
                                        class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>
<div class="modal modal-blur fade" id="NewSlider" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-fullscreen"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create a new blog post</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form enctype="multipart/form-data" action="{{ route('NewRecord') }}"
                    method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label"> Brief Title </label>
                        <input type="text" class="form-control" name="Title">
                    </div>





                    <input required type="hidden" name="created_at"
                        value="{{ date('Y-m-d H:i:s') }}">

                    <input required type="hidden" name="TableName" value="blogs">


                    <div class="mb-3">
                        <label class="form-label">Date of Creation

                            (Y-m-d format strictly | Applies to update as well)

                        </label>
                        <input id="litepicker" required type="text"
                            class="form-control DateArea" name="created_at">
                    </div>



                    <div class="mb-3">
                        <label class="form-label">Thumbnail</label>
                        <input required type="file" class="form-control" name="URL">
                    </div>


                    <div class="mb-3">
                        <label class="form-label">Post Content</label>
                        <textarea name="Desc" class="form-control tiny" id="tiny"></textarea>
                    </div>



                    {!! Form::hidden($name = 'UID', $value = \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')), [($options = null)]) !!}




                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('cms.ViewBlog')



@isset($Blogs)
    @foreach ($Blogs as $data)
        <form novalidate action="{{ route('CMSUpdate') }}" class=""
            method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="blogs">


                {{ RunUpdateModal($ModalID = $data->id, $Extra = '<div class="mb-10"> <label for="" class="required form-label">Thumbnail</label> <input required type="file" class="form-control form-control-solid" name="URL" />', $csrf = null, $Title = 'Update the selected  record', $RecordID = $data->id, $col = '12', $te = '12', $TableName = 'blogs') }}
            </div>
        </form>
    @endforeach
@endisset
