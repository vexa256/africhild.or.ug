<div class="modal modal-blur fade" id="NewSlider" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-fullscreen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create news post</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form enctype="multipart/form-data" action="{{ route('NewRecord') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label"> Brief Title </label>
                        <input type="text" class="form-control" name="Title">
                    </div>





                    <input required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">

                    <input required type="hidden" name="TableName" value="news">


                    <div class="mb-3">
                        <label class="form-label">Thumbnail</label>
                        <input required type="file" class="form-control" name="URL">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">News Content</label>
                        <textarea name="Desc" class="form-control tiny" id="tiny"></textarea>
                    </div>



                    {!! Form::hidden($name = 'UID', $value = \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')), [($options = null)]) !!}




                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
