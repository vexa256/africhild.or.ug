<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewSlider" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                Add news post
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            @isset($Title)
                {{ $Desc }} | Only Portrait images are supported
            @endisset
        </h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Title </th>
                        <th>Content </th>
                        <th>Thumbnail </th>
                        <th>Preview </th>
                        <th>Edit </th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($News)
                        @foreach ($News as $data)
                            <tr>
                                <td> {{ $data->Title }} </td>
                                <td>

                                    <a class="btn btn-dark btn-sm" data-bs-toggle="modal" href="#News{{ $data->id }}">

                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> View
                                    </a>



                                </td>
                                <td>

                                    <a class="btn btn-dark btn-sm" data-lightbox="image-1" data-title="My caption"
                                        href="{{ asset($data->URL) }}">

                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> View
                                    </a>



                                </td>

                                <td><a href="{{ url('/') }}" class="btn btn-sm btn-dark">
                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> Preview
                                    </a></td>

                                {{ UpdateCms($data) }}

                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'news']) }}"
                                        data-msg="You want to delete this record. This action is not reversible" href="#"
                                        class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('cms.NewNews')
@include('cms.ViewNews')


@isset($News)
    @foreach ($News as $data)
        <form novalidate action="{{ route('CMSUpdate') }}" class="" method="POST"
            enctype="multipart/form-data">
            @csrf

            <div class="row">
                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="news">


                {{ RunUpdateModal($ModalID = $data->id,$Extra ='<div class="mb-10"> <label for="" class="required form-label">Thumbnail</label> <input required type="file" class="form-control form-control-solid" name="URL" />',$csrf = null,$Title = 'Update the selected  record',$RecordID = $data->id,$col = '12',$te = '12',$TableName = 'news') }}
            </div>
        </form>
    @endforeach
@endisset
