<div class="modal modal-blur fade" id="NewSlider" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add a new research associates record record</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form enctype="multipart/form-data" action="{{ route('NewRecord') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label"> Name</label>
                        <input type="text" class="form-control" name="Name">
                    </div>



                    <div class="mb-3">
                        <label class="form-label"> Title</label>
                        <input type="text" class="form-control" name="Title">
                    </div>


                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <textarea name="Desc" class="form-control"></textarea>
                    </div>


                    <input required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">

                    <input required type="hidden" name="TableName" value="assocs">

                    <div class="mb-3">
                        <label class="form-label">Upload Thumbnail</label>
                        <input required type="file" class="form-control" name="URL">
                    </div>



                    {!! Form::hidden($name = 'UID', $value = \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')), [($options = null)]) !!}




                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
