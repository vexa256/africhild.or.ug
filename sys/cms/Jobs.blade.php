<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewSlider" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New job opening
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Desc }} | Only PDF files are supported
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Title </th>
                        <th>PDF </th>
                        <th>Preview </th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Jobs)
                        @foreach ($Jobs as $data)
                            <tr>
                                <td>
                                    @if ($data->Title == '')
                                        N/A

                                    @else

                                        {{ $data->Title }}
                                    @endif
                                </td>

                                <td>
                                    <a data-doc="  {{ $data->Title }} " data-source="{{ asset($data->URL) }}"
                                        data-bs-toggle="modal" href="#PdfJS" class="btn btn-sm  PdfViewer btn-primary"> <i
                                            class="fas me-1  fa-file-pdf" aria-hidden="true"></i> View
                                    </a>
                                </td>

                                <td><a href="{{ url('/') }}" class="btn btn-sm btn-dark">
                                        <i class="fas me-1 fa-binoculars" aria-hidden="true"></i> Preview
                                    </a></td>



                                <td>

                                    <a data-route="{{ route('MassDelete', ['id' => $data->id, 'TableName' => 'jobs']) }}"
                                        data-msg="You want to delete this record. This action is not reversible" href="#"
                                        class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('cms.NewJob')
@include('cms.Pdf')
