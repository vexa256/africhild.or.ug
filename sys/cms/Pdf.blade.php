<div class="modal fade" id="PdfJS">
    <div class="modal-dialog modal-dialog-scrollable modal-fullscreen">
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <h5 class="modal-title">CMS Settings Advanced PDF Documentation Viewer

                </h5>

                <!--begin::Close-->
                <a href="#" type="button" class="btn btn-info" data-bs-dismiss="modal"
                    class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-2x fa-times" aria-hidden="true"></i>
                </a>
                <!--end::Close-->
            </div>

            <div class="modal-body " style="height: 500px !important">

                {{-- The pdf is injected here by the js file --}}

                <div id="adobe-dc-view"></div>



            </div>



        </div>
    </div>
</div>
