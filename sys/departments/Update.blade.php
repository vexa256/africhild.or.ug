@isset($Depts)
    @foreach ($Depts as $data)
        <div class="modal modal-blur fade" id="UpdateModal{{ $data->id }}" tabindex="-1" role="dialog"
            aria-hidden="true">

            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Update the department <span class="text-danger font-weight-bold">
                                {{ $data->Department }}

                            </span>

                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('UpdateLogic') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label class="form-label">Department</label>
                                <input type="text" class="form-control" name="Department"
                                    value="{{ $data->Department }}">
                            </div>

                            <input type="hidden" name="id" value="{{ $data->id }}">
                            <input type="hidden" name="TableName" value="departments">

                            <div class="modal-footer">
                                <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                                    Cancel
                                </a>
                                <button type="submit" class="btn btn-orange btn-pill  ms-auto">
                                    <i class="fas me-1 fa-check" aria-hidden="true"></i>
                                    Save Changes
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
