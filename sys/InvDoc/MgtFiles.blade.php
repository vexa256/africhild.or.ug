<div class="row">
    <div class="col-auto ms-auto float-end mb-3">
        <div class="btn-list">

            <a href="#NewDoc" data-bs-toggle="modal" class="btn btn-dark  btn-sm">
                <i class="fa fa-plus me-1" aria-hidden="true"></i>

                New File
            </a>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@isset($Title)
                {{ $Title }}
            @endisset</h3>
    </div>


    <div class="card-body  ">
        <div class="table-responsive  ">
            <table class="px-1 py-1 mytable table  table-stripped table-bordered">
                <thead>
                    <tr>
                        <th>Category </th>
                        <th>Title </th>
                        <th>Description </th>
                        <th>View File </th>
                        <th>Update</th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>

                    @isset($Docs)
                        @foreach ($Docs as $data)
                            <tr>
                                <td>{{ $data->Category }}</td>
                                <td>{{ $data->Title }}</td>
                                <td>{{ $data->Description }}</td>

                                <td>

                                    <a data-doc="{{ $data->Title }}  ({{ $CatName }})"
                                        data-source="{{ asset($data->Url) }}" data-bs-toggle="modal" href="#PdfJS"
                                        class="btn  PdfViewer btn-dark btn-sm btn-shadow">
                                        <i class="fas me-1  fa-binoculars" aria-hidden="true"></i>
                                        View </a>
                                </td>
                                <td>

                                    <a data-bs-toggle="modal" href="#UpdateDoc{{ $data->id }}"
                                        class="btn  btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-edit" aria-hidden="true"></i>
                                        update </a>
                                </td>
                                <td>

                                    <a data-route="{{ route('DeleteDoc', ['id' => $data->id]) }}"
                                        data-msg="You want to delete this inventory documentation record. This action is not reversible"
                                        href="#" class="btn deleteConfirm btn-orange btn-sm btn-shadow">
                                        <i class="fas me-1  fa-trash" aria-hidden="true"></i>
                                        Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endisset


                </tbody>

            </table>
        </div>
    </div>

</div>

@include('InvDoc.NewDoc')
@include('InvDoc.DocViewer')
@include('InvDoc.Update')
