<div class="card shadow-lg">
    <div class="card-header">
        <h2 class="card-title">
            {{ $Title }}
        </h2>
    </div>

    <div class="card-body">
        <form action="{{ route('DocSelected') }} " method="POST">
            @csrf
            <div class="row">

                <div class="col-md-12">
                    <div class="mb-3">
                        <div class="form-label">Select File Category</div>
                        <select name="CatID" class="form-select flexselect form-control">
                            <option value=""></option>
                            @isset($Cats)
                                @foreach ($Cats as $data)
                                    <option value="{{ $data->CatID }}">
                                        {{ $data->Category }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                </div>

                <div class="col-12">
                    <div class="mb-3 mt-3 float-end">
                        <button type="submit" class="btn btn-danger">
                            Attach Documentation
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
