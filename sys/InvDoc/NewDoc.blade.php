<div class="modal modal-blur fade" id="NewDoc" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Attach a new inventory document to the category
                    <span class="text-danger">{{ $CatName }}</span>
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form enctype="multipart/form-data" action="{{ route('AddFile') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label">Title</label>
                        <input required type="text" class="form-control" name="Title">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <input required type="text" class="form-control" name="Description">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">PDF File</label>
                        <input accept="application/pdf" required type="file" class="form-control" name="Url">
                    </div>

                    {!! Form::hidden($name = 'DocID', $value = \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')), [($options = null)]) !!}


                    <input required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">

                    <input required type="hidden" name="CatID" value="{{ $CatID }}">








                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
