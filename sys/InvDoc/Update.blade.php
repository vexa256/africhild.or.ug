@isset($Docs)
    @foreach ($Docs as $data)
        <div class="modal modal-blur fade" id="UpdateDoc{{ $data->id }}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update the selected inventory documentation record with title
                            <span class="text-danger">{{ $data->Title }}</span>
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form enctype="multipart/form-data" action="{{ route('UpdateDocs') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label class="form-label">Title</label>
                                <input required type="text" class="form-control" name="Title"
                                    value="{{ $data->Title }}">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Description</label>
                                <input required type="text" class="form-control" name="Description"
                                    value="{{ $data->Description }}">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">PDF File</label>
                                <input accept="application/pdf" type="file" class="form-control" name="Url">
                            </div>



                            <input required type="hidden" name="id" value="{{ $data->id }}">

                            <input required type="hidden" name="TableName" value="inventory_docs">








                            <div class="modal-footer">
                                <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                                    Cancel
                                </a>
                                <button type="submit" class="btn btn-orange btn-pill ms-auto">
                                    <i class="fas me-1 fa-check" aria-hidden="true"></i>
                                    Save Changes
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
