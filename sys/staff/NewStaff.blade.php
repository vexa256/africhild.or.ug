<div class="modal modal-blur fade" id="NewStaff" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create new staff member record</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="{{ route('NewStaff') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label">Staff Name</label>
                        <input required type="text" class="form-control" name="Name">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Email <small class="text-danger">(This email is not editable
                                after creation)</small>
                        </label>
                        <input required type="email" class="form-control" name="Email">
                    </div>


                    <input required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">

                    <div class="mb-3">
                        <div class="form-label">Account Privileges</div>
                        <select name="Privileges" class="form-select flexselect form-control">
                            <option value=""></option>
                            @if (Auth::user()->Privileges == 'Super_Admin')
                                <option value="Super_Admin">Super Admin</option>
                            @endif
                            <option value="Admin">Admin</option>
                            <option value="User">User</option>

                        </select>
                    </div>


                    <div class="mb-3">
                        <div class="form-label">Staff Role</div>
                        <select name="RID" class="form-select flexselect form-control">
                            <option value=""></option>
                            @isset($Roles)
                                @foreach ($Roles as $role)
                                    <option value="{{ $role->RID }}">
                                        {{ $role->Role }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                    </div>



                    <div class="mb-3">
                        <div class="form-label">Department</div>
                        <select name="DID" class="form-select flexselect form-control">
                            <option value=""></option>
                            @isset($Depts)
                                @foreach ($Depts as $Dept)
                                    <option value="{{ $Dept->DID }}">
                                        {{ $Dept->Department }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                    </div>



                    {!! Form::hidden($name = 'EmpID', $value = \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')), [($options = null)]) !!}

                    <div class="modal-footer">
                        <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-orange btn-pill ms-auto">
                            <i class="fas me-1 fa-check" aria-hidden="true"></i>
                            Save Changes
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
