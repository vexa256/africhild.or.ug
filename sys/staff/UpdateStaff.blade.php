@isset($StaffDetails)
    @foreach ($StaffDetails as $data)
        <div class="modal modal-blur fade" id="UpdateStaff{{ $data->id }}" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update the selected staff member

                            <span class="text-danger">
                                {{ $data->Name }}
                            </span>
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('UpdateLogic') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label class="form-label">Staff Name</label>
                                <input required value="{{ $data->Name }}" type="text" class="form-control" name="Name">
                            </div>

                            <div class="mb-3">
                                <div class="form-label">Account Privileges</div>
                                <select required name="Privileges" class="form-select flexselect form-control">
                                    <option value="{{ $data->Privileges }}">{{ $data->Privileges }}</option>
                                    <option value="Super_Admin">Super Admin</option>
                                    <option value="Admin">Admin</option>
                                    <option value="User">User</option>

                                </select>
                            </div>


                            <input required type="hidden" name="id" value="{{ $data->id }}">
                            <input required type="hidden" name="TableName" value="staff">
                            <input required type="hidden" name="EmpID" value="{{ $data->EmpID }}">

                            <div class="mb-3">
                                <div class="form-label">Staff Role</div>
                                <select name="RID" class="form-select flexselect form-control">
                                    <option value="{{ $data->RID }}">{{ $data->Role }}</option>
                                    @isset($Roles)
                                        @foreach ($Roles as $role)
                                            <option value="{{ $role->RID }}">
                                                {{ $role->Role }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>



                            <div class="mb-3">
                                <div class="form-label">Department</div>
                                <select name="DID" class="form-select flexselect form-control">
                                    <option value="{{ $data->DID }}">{{ $data->Department }}</option>

                                    @isset($Depts)
                                        @foreach ($Depts as $Dept)
                                            <option value="{{ $Dept->DID }}">
                                                {{ $Dept->Department }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>




                            <div class="modal-footer">
                                <a href="#" class="btn btn-pill btn-dark" data-bs-dismiss="modal">
                                    Cancel
                                </a>
                                <button type="submit" class="btn btn-orange btn-pill ms-auto">
                                    <i class="fas me-1 fa-check" aria-hidden="true"></i>
                                    Save Changes
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
